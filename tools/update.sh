#!/usr/bin/env bash
#  -----------------------------------------------------------------------------
#  Copyleft 2020-2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  -----------------------------------------------------------------------------
set -e

if [ $# -lt 2 ]; then
    echo "Usage: $(basename $0) VERSION CODENAME"
    exit 1
fi

VERSION="${1}"
CODENAME="${2}"

if [[ $VERSION =~ ^[0-9]+(\.[0-9]+)+ ]] ; then
    echo -e "\e[33;1mUpdate GEM version to ${VERSION}\e[39;0m"

    echo -e "==>\e[36;1m translation.sh \e[39;0m"
    sed "s/--package-version=\"\([^$]*\)\"/--package-version=\"${VERSION}\"/g" \
        -i tools/translation.sh

    echo -e "==>\e[36;1m setup.py \e[39;0m"
    sed "s/version='\([^$]*\)'/version='${VERSION}'/g" \
        -i setup.py

    echo -e "==>\e[36;1m gem.desktop \e[39;0m"
    sed "s/Version=\([^$]*\)/Version=${VERSION}/g" \
        -i geode_gem/data/desktop/gem.desktop

    echo -e "==>\e[36;1m geode_gem/api.py \e[39;0m"
    sed "s/Version = \"\([^$]*\)\"/Version = \"${VERSION}\"/g" \
        -i geode_gem/engine/api.py

    echo -e "==>\e[36;1m geode_gem/data/config/metadata.conf \e[39;0m"
    sed "s/version = \([^$]*\)/version = ${VERSION}/g" \
        -i geode_gem/data/config/metadata.conf
    sed "s/code_name = \([^$]*\)/code_name = ${CODENAME}/g" \
        -i geode_gem/data/config/metadata.conf

else
    echo "Version must be x.y[.z][...]"
fi
