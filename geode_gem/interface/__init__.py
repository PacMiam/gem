# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _
from typing import NoReturn, Self

from gi.repository import Gio, Gtk
from gi.repository.GLib import Error, idle_add, source_remove

from geode_gem.core.metadata import Metadata
from geode_gem.core.utils import get_data
from geode_gem.interface.utils import (
    block_interface_signals,
    convert_state_to_glib_variant,
    generate_blank_icons,
    icon_from_data,
    magic_from_file,
)
from geode_gem.interface.headerbar import HeaderBar
from geode_gem.interface.infobar import InfoBar
from geode_gem.interface.listbox import ListBoxCollections, ListBoxGames
from geode_gem.interface.properties import GeodeGemGameProperties
from geode_gem.interface.revealer import Revealer
from geode_gem.interface.screenshots import GeodeGemScreenshots
from geode_gem.interface.sidebar import Sidebar
from geode_gem.interface.textview import GeodeGemTextViewer
from geode_gem.interface.toolbar import ToolbarCollections, ToolbarGames


class GeodeGemInterface(Gtk.ApplicationWindow):
    """Manage interface related methods and variables

    Attributes
    ----------
    actions : dict
        Store the Gio.Action generate by the application
    application : geode_gem.application.Application
        The instance of the main application
    config : geode_gem.configuration.Configuration
        The instance of the configuration manager for the current profile
    has_debug : bool
        The value of the debug flag as defined by the main application
    logger : logging.Logger
        The instance of the logger
    signals : dict
        Store the widget signals
    threads : dict
        Store the running threads by category
    """

    def __init__(self, *args, **kwargs):
        """Constructor"""

        super().__init__(*args, **kwargs)

        self.application = kwargs.get("application")
        self.config = self.application.config
        self.logger = self.application.logger
        self.has_debug = self.application.has_debug

        self.actions = dict()
        self.signals = dict()
        self.threads = dict(games=int())

    def __init_widgets__(self):
        """Initialize the widget for the main interface"""

        version_subtitle = f"Version {Metadata.VERSION} – {Metadata.CODENAME}"
        if self.has_debug:
            version_subtitle += " – Debug"

        self.set_default_size(1024, 768)
        self.set_default_icon_name(Metadata.Python.APPLICATION)

        self.icons_theme = Gtk.IconTheme.get_default()
        self.icons_theme.append_search_path(str(get_data("data", "consoles")))
        self.icons_theme.append_search_path(str(get_data("data", "icons")))

        self.icons_blank = generate_blank_icons()

        self.settings = Gtk.Settings.get_default()

        self.headerbar = HeaderBar(
            self,
            title=Metadata.NAME,
            subtitle=version_subtitle,
            show_close_button=True,
        )
        self.set_titlebar(self.headerbar)

        self.grid_collections = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.grid_games = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)

        self.scrolled_collections = Gtk.ScrolledWindow()
        self.scrolled_games = Gtk.ScrolledWindow()
        self.scrolled_sidebar = Gtk.ScrolledWindow(no_show_all=True)

        self.paned_collection = Gtk.Paned(
            orientation=Gtk.Orientation.HORIZONTAL, wide_handle=True
        )
        self.paned_games = Gtk.Paned(
            orientation=Gtk.Orientation.VERTICAL, wide_handle=False
        )

        self.toolbar_collections = ToolbarCollections(
            self, border_width=4, spacing=12
        )
        self.toolbar_games = ToolbarGames(self, border_width=4, spacing=12)

        self.collections = ListBoxCollections(self)
        self.games = ListBoxGames(self, activate_on_single_click=False)
        self.sidebar = Sidebar(self)

        self.infobar_games = InfoBar(
            self,
            message_type=Gtk.MessageType.ERROR,
            revealed=False,
            show_close_button=True,
        )

        self.revealer_games = Revealer(
            self,
            valign=Gtk.Align.START,
            halign=Gtk.Align.CENTER,
            transition_type=Gtk.RevealerTransitionType.SLIDE_DOWN,
        )

        self.overlay_games = Gtk.Overlay()
        self.overlay_games.add_overlay(self.revealer_games)

    def __connect_widgets__(self):
        """Connect Gtk signal to the interface widgets"""

        self.connect("delete-event", self.stop)

        self.headerbar.connect_widgets()
        self.toolbar_collections.connect_widgets()
        self.toolbar_games.connect_widgets()

        self.collections.connect_widgets()

        self.signals = self.games.connect_widgets(self.signals)

    def __packing_widgets__(self):
        """Packing together the interface widgets"""

        self.scrolled_collections.add(self.collections)
        self.scrolled_games.add(self.games)
        self.scrolled_sidebar.add(self.sidebar)

        self.grid_collections.add(self.toolbar_collections)
        self.grid_collections.pack_start(
            self.scrolled_collections, True, True, 0
        )

        self.paned_collection.set_position(300)
        self.paned_collection.pack1(self.grid_collections, False, False)
        self.paned_collection.add2(self.grid_games)

        self.paned_games.pack1(self.overlay_games, True, True)
        self.paned_games.pack2(self.scrolled_sidebar, False, False)

        self.overlay_games.add(self.scrolled_games)

        self.grid_games.add(self.toolbar_games)
        self.grid_games.add(self.infobar_games)
        self.grid_games.pack_start(self.paned_games, True, True, 0)

        self.add(self.paned_collection)

    def __customize_from_configuration__(self):
        """Load the configuration file to customize the main application"""

        self.set_sidebar_icon_mode(
            self.config.get("sidebar", "icon-mode", fallback="both")
        )
        self.set_sidebar_orientation(
            self.config.get("sidebar", "orientation", fallback="horizontal")
        )
        self.set_sidebar_visibility(
            self.config.getboolean("sidebar", "visible", fallback=True)
        )

        self.toggle_dark_theme(
            self.config.getboolean(
                "theme",
                "is-dark",
                fallback=self.get_setting("gtk-application-prefer-dark-theme"),
            )
        )

        self.set_games_sorted_column(self.config.games_sorted_column)
        self.set_games_sorted_order(self.config.games_sorted_order)

    def block_signals(self):
        """Block check button signals to avoid stack overflow when toggled"""

        for widget, signals in self.signals.items():
            for signal in signals:
                widget.handler_block(signal)

    def unblock_signals(self):
        """Unblock check button signals"""

        for widget, signals in self.signals.items():
            for signal in signals:
                widget.handler_unblock(signal)

    def add_image_in_cache(self, size, filepath, identifier):
        """Insert a new image in the cache directory

        Parameters
        ----------
        size : int
            The size of the image to store in the cache directory
        filepath : pathlib.Path
            The path of the image file to store in the cache directory
        identifier : str
            The identifier to use as filename for the stored image

        Returns
        -------
        gi.repository.GdkPixbuf.Pixbuf or None
            The new pixbuf instance if the image have been successfully
            registered in the cache directory, None otherwise
        """

        if not magic_from_file(filepath, mime=True).startswith("image/"):
            raise TypeError(f"{filepath} is not a image")

        path = self.application.cache_directory.joinpath("images", str(size))
        path.mkdir(mode=0o755, parents=True, exist_ok=True)

        cache_path = path.joinpath(f"{identifier}.png")

        try:
            icon = icon_from_data(filepath, width=size, height=size)
            icon.savev(str(cache_path), "png", list(), list())

            return icon

        except Error:
            return None

    def append_games_from_collection(self, collection):
        """Add games from specified collection in the games listbox

        Parameters
        ----------
        collection : geode_gem.core.collection.Collection
            The collection where the games will be retrieved

        Yields
        ------
        bool
            True if the process still in progress, False otherwise
        """

        self.__selection__ = {
            "collection": collection.identifier,
            "game": None,
        }

        self.games.reset()

        self.revealer_games.set_reveal_child(True)
        self.revealer_games.spinner.start()
        yield True

        self.games.freeze_child_notify()
        yield True

        for element in self.application.get_games_for_collection(collection):
            if not isinstance(element, int):
                self.games.thaw_child_notify()
                self.games.add_row(element)
                self.games.freeze_child_notify()

            yield True

        self.threads["games"] = 0

        self.revealer_games.set_reveal_child(False)
        self.revealer_games.spinner.stop()
        yield True

        self.toolbar_games.set_filtering_widgets(True)
        self.games.thaw_child_notify()
        yield False

    def get_filter_text_from_game_toolbar_entry(self):
        """Retrieve the content of the filtering searchentry from games toolbar

        Returns
        -------
        str
            The content of the searchentry
        """

        return self.toolbar_games.searchentry.get_text().strip()

    def get_pixbuf_from_cache(self, size, identifier):
        """Retrieve a pixbuf object from the cache directory

        Parameters
        ----------
        size : int
            The size of the image to retrieve from cache
        identifier : str
            The identifier used to store the image inside the cache

        Returns
        -------
        gi.repository.GdkPixbuf.Pixbuf or None
            The new pixbuf instance if the image exists in cache, None otherwise
        """

        path = self.application.cache_directory.joinpath(
            "images", str(size), f"{identifier}.png"
        )

        if path.exists():
            return icon_from_data(path, width=size, height=size)

    def get_setting(self, option):
        """Retrieve the value of the specified setting option

        Parameters
        ----------
        option : str
            The name of the option from the gsettings storage

        Returns
        -------
        object
            The value of this option
        """

        return self.settings.get_property(option)

    def on_edit_game_properties(self, *args, **kwargs):
        """Open the game properties editor dialog for the selected game

        See Also
        --------
        geode_gem.interface.properties.GeodeGemGameProperties
        """

        game = self.selected_game

        width, height = self.application.get_window_size("game-properties")

        dialog = GeodeGemGameProperties(
            self,
            game,
            default_height=height,
            default_width=width,
            title=_("Game properties"),
        )

        response = dialog.run()
        size = dialog.get_size()
        dialog.destroy()

        if response == Gtk.ResponseType.APPLY:
            self.__selection__["game"] = game
            self.application.save_game(game, dialog.data)
            self.remove_cache_for_game(game)
            self.update_game(game)

        self.application.set_window_size("game-properties", *size)

    def on_open_notes_viewer(self, *args, **kwargs):
        """Open the notes viewer for the selected game

        See Also
        --------
        geode_gem.interface.textview.GeodeGemTextViewer
        """

        width, height = self.application.get_window_size("game-notes")

        game = self.selected_game

        dialog = GeodeGemTextViewer(
            self,
            game.note_path,
            _("Notes viewer"),
            default_height=height,
            default_width=width,
            read_only=False,
            title=game.label,
        )

        response = dialog.run()
        if response == Gtk.ResponseType.APPLY:
            text = dialog.get_text()

            if len(text) == 0:
                game.note_path.unlink()
                self.application.logger.debug(
                    f"The note for the game '{game.identifier}' have been "
                    "removed from the disk"
                )

            else:
                if not game.note_path.parent.exists():
                    game.note_path.parent.mkdir(
                        mode=0o750, parents=True, exist_ok=True
                    )

                game.note_path.write_text(text)
                self.application.logger.debug(
                    f"Write {len(text)} character(s) for the notes of the "
                    f"game '{game.identifier}'"
                )

        size = dialog.get_size()
        dialog.destroy()

        self.application.set_window_size("game-notes", *size)

    def on_open_log_viewer(self, *args, **kwargs):
        """Open the log viewer for the selected game

        See Also
        --------
        geode_gem.interface.textview.GeodeGemTextViewer
        """

        width, height = self.application.get_window_size("game-log")

        game = self.selected_game

        dialog = GeodeGemTextViewer(
            self,
            game.log_path,
            _("Log file viewer"),
            default_height=height,
            default_width=width,
            read_only=True,
            title=game.label,
        )

        dialog.run()
        size = dialog.get_size()
        dialog.destroy()

        self.application.set_window_size("game-log", *size)

    def on_open_screenshots_viewer(self, *args, **kwargs):
        """Open the screenshots viewer for the selected game

        See Also
        --------
        geode_gem.interface.screenshots.GeodeGemScreenshots
        """

        width, height = self.application.get_window_size("game-screenshots")

        dialog = GeodeGemScreenshots(
            self,
            default_height=height,
            default_width=width,
            title=self.selected_game.label,
        )

        dialog.run()
        size = dialog.get_size()
        dialog.destroy()

        self.application.set_window_size("game-screenshots", *size)

    @block_interface_signals
    def on_select_collection(self, listbox, row):
        """Select a new collection from the listbox

        Parameters
        ----------
        listbox : geode_gem.interface.listbox.ListBoxCollections
            The listbox which contains the collections
        row : gi.repository.Gtk.ListBoxRow
            The selected row from the specified listbox
        """

        collection = row.geode_item

        if not self.threads.get("games", 0) == 0:
            source_remove(self.threads["games"])

        self.sidebar.reset()
        self.toolbar_games.reset()
        self.infobar_games.set_revealed(False)

        # Avoid to reload the selected collection
        if not self.selected_collection == collection.identifier:
            self.toolbar_games.set_filtering_widgets(False)
            self.games.reset()

            self.threads["games"] = idle_add(
                self.append_games_from_collection(collection).__next__
            )

    @block_interface_signals
    def on_select_game(self, listbox, row):
        """Select a new game from the listbox

        Notes
        -----
        The sidebar will be updated with the selected game

        Parameters
        ----------
        listbox : geode_gem.interface.listbox.ListBoxGames
            The listbox which contains the games
        row : gi.repository.Gtk.ListBoxRow
            The selected row from the specified listbox
        """

        game = row.geode_item
        selected_game = self.selected_game

        self.__selection__["game"] = game

        if (
            selected_game is None
            or not selected_game.identifier == game.identifier
        ):
            self.sidebar.update(game, self.selected_launcher)
            self.toolbar_games.update(game, self.selected_launcher)

    def register_action(self, name, parameter_type=None, state=None):
        """Register an action with a specific name

        Parameters
        ----------
        name : str
            The name of the action
        parameter_type : gi.repository.GLib.VariantType or None, default: None
            The type of the parameter used to activate the signal
        state : gi.repository.GLib.Variant or type, default: None
            The initial state of the action

        See Also
        --------
        gi.repository.Gio.SimpleAction
        """

        if state is not None:
            state = convert_state_to_glib_variant(state)

            action = Gio.SimpleAction.new_stateful(name, parameter_type, state)
        else:
            action = Gio.SimpleAction.new(name, parameter_type)

        self.add_action(action)
        self.actions[name] = action

        return action

    def remove_cache_for_game(self, game) -> None:
        """Remove the cached content for the specified game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game
        """

        for size in (16,):
            path = self.application.cache_directory.joinpath(
                "images", str(size), f"{game.checksum}.png"
            )
            path.unlink(missing_ok=True)

    def run(self):
        """Start the main interface and run the main loop"""

        self.__selection__ = dict(collection=None, game=None)

        self.__init_widgets__()
        self.__connect_widgets__()
        self.__packing_widgets__()

        self.__customize_from_configuration__()

        for collection in self.application.get_collections():
            self.collections.add_row(collection)

        self.show_all()

    @property
    def selected_collection(self):
        """Returns the currently selected collection

        Returns
        -------
        geode_gem.core.collection.GenericCollection or None
            The selected collection or None otherwise
        """

        return self.__selection__.get("collection", None)

    @property
    def selected_game(self):
        """Returns the currently selected game

        Returns
        -------
        geode_gem.core.game.GenericGame or None
            The selected game or None otherwise
        """

        return self.__selection__.get("game", None)

    @property
    def selected_launcher(self):
        """Returns the launcher associate with the currently selected game

        Returns
        -------
        geode_gem.core.launcher.GenericLauncher or None
            The launcher related to the selected game, None otherwise
        """

        if (game := self.selected_game) is not None:
            return self.application.get_launcher_from_game(game)

        return None

    def set_games_sorted_column(self, sort_column):
        """Define the sorted column of the games list

        Parameters
        ----------
        sort_column : geode_gem.configuration.GamesColumns or str
            The enum object related to the user configuration choice
        """

        self.games.set_sorted_column(sort_column)

    def set_games_sorted_order(self, sort_order):
        """Define the sorted order of the games list

        Parameters
        ----------
        sort_order : gi.repository.Gtk.SortType or str
            The enum object related to the user configuration choice
        """

        self.games.set_sorted_order(sort_order)

    def set_setting(self, option, value):
        """Redefine a new value for the specific setting option

        Parameters
        ----------
        option : str
            The name of the option from the gsettings storage
        value : object
            The new value for this option
        """

        self.settings.set_property(option, value)

    def set_sidebar_icon_mode(
        self: Self, icon_mode: Gtk.ImageType | str
    ) -> NoReturn:
        """Define the game icon of the sidebar dedicated to game information

        Parameters
        ----------
        icon_mode : gi.repository.Gtk.ImageType or str
            The mode of the game icon visibility
        """

        if isinstance(icon_mode, str):
            match icon_mode:
                case "none":
                    icon_mode = Gtk.ImageType.EMPTY
                case "icon-only":
                    icon_mode = Gtk.ImageType.ICON_NAME
                case _:
                    icon_mode = Gtk.ImageType.PIXBUF

        self.sidebar.set_icon_mode(icon_mode)

        if self.selected_game is not None:
            self.sidebar.update(self.selected_game, self.selected_launcher)

    def set_sidebar_orientation(self, orientation):
        """Define the orientation of the sidebar dedicated to game information

        Parameters
        ----------
        position : gi.repository.Gtk.Orientation or str
            The orientation of the sidebar
        """

        if isinstance(orientation, str):
            match orientation:
                case "vertical":
                    orientation = Gtk.Orientation.VERTICAL
                case _:
                    orientation = Gtk.Orientation.HORIZONTAL

        if orientation == Gtk.Orientation.VERTICAL:
            self.scrolled_sidebar.set_size_request(-1, 240)
        elif orientation == Gtk.Orientation.HORIZONTAL:
            self.scrolled_sidebar.set_size_request(350, -1)

        self.paned_games.set_orientation(orientation)
        self.sidebar.set_orientation(orientation)

    def set_sidebar_visibility(self, status):
        """Define the visibility of the sidebar dedicated to game information

        Parameters
        ----------
        status : bool
            The status of the sidebar visibility
        """

        self.scrolled_sidebar.set_visible(status)

    def set_state_for_action(self, name, state=None):
        """Redefine a new value for a specific action

        Parameters
        ----------
        name : str
            The name of the action
        state : gi.repository.GLib.Variant or type, default: None
            The new state of the action

        See Also
        --------
        gi.repository.Gio.SimpleAction.set_state
        """

        if name in self.actions:
            state = convert_state_to_glib_variant(state)

            self.actions.get(name).set_state(state)

    def show_game_error_message(self, game, exception):
        """Update the revealer to show the error occurs during game crash

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to retrieve the information
        exception : Exception
            The exception raised during the crash
        """

        if isinstance(exception, MemoryError):
            message = _("The game was stopped by raising a memory error")
        elif isinstance(exception, OSError):
            message = _(
                "There is a problem with this game. You can read the log "
                "output to see what is wrong."
            )
        else:
            message = _("An unknown error occurs during the process")

        self.infobar_games.set_informational_message(
            _("%(game_label)s: %(infobar_error_message)s")
            % {
                "game_label": game.label,
                "infobar_error_message": message,
            },
            Gtk.MessageType.ERROR,
        )
        self.infobar_games.set_revealed(True)

    def stop(self, *args, **kwargs):
        """Stop the main interface and quit the main loop"""

        self.hide()

        self.application.on_quit()

    def toggle_dark_theme(self, is_dark):
        """Switch between the light and dark theme variant

        Parameters
        ----------
        is_dark : bool
            True if the wanted variant is the dark one, False otherwise
        """

        self.set_setting("gtk-application-prefer-dark-theme", is_dark)
        self.config.update_option("theme", "is-dark", is_dark)

    def update_game(self, game):
        """Update some widget with the specified game information

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to retrieve the information
        """

        # If the selected collection if not the one which own the specified
        # game, there is no need to go futher
        if not self.selected_collection == game.collection.identifier:
            return

        # The sidebar needs to be updated only when the game is selected
        if self.selected_game == game:
            self.toolbar_games.update(game, self.selected_launcher)
            self.sidebar.update(game, self.selected_launcher)

        self.games.update_game(game)
