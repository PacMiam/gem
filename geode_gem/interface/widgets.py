# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gtk

from geode_gem.interface.utils import add_style_class_to_widget


class FramedListBox(Gtk.Frame):
    """Represents a framed ListBox

    Attributes
    ----------
    listbox : gi.repository.Gtk.ListBox
        The inner listbox widget

    See Also
    --------
    gi.repository.Gtk.Frame
    gi.repository.Gtk.ListBox
    """

    def __init__(
        self,
        *args,
        activate_on_single_click=False,
        selection_mode=Gtk.SelectionMode.NONE,
        **kwargs,
    ):
        """Constructor

        Parameters
        ----------
        activate_on_single_click : bool, optional
            Activate the listbox item with a single click (disabled by default)
        selection_mode : gi.repository.Gtk.SelectionMode, optional
            Set the selection mode for the listbox (no selection by default)

        See Also
        --------
        gi.repository.Gtk.ListBox
        """

        super().__init__()

        self.listbox = Gtk.ListBox(
            activate_on_single_click=activate_on_single_click,
            selection_mode=selection_mode,
            **kwargs,
        )

        self.add(self.listbox)


class ListBoxOption(Gtk.Box):
    """Represents a configurator option to add in a ListBox

    Attributes
    ----------
    box : gi.repository.Gtk.Box
        The inner grid which contains the label and the icon
    label : gi.repository.Gtk.Label
        The inner label widget which contains the name of the option
    image : gi.repository.Gtk.Image
        The inner image widget which represents the help text if available

    See Also
    --------
    gi.repository.Gtk.Box
    gi.repository.Gtk.Image
    gi.repository.Gtk.Label
    """

    def __init__(self, label, widget, expand=True, help_text=None):
        """Constructor

        Parameters
        ----------
        label : str
            The label of the option
        widget : gi.repository.Gtk.Widget
            The widget to insert in the inner grid
        expand : bool, optional
            Expand horizontally the specified widget in the inner grid
        help_text : str, optional
            Add an icon next to the label with a tooltip containing this text
        """

        super().__init__(
            homogeneous=True,
            margin_bottom=6,
            margin_end=6,
            margin_start=6,
            margin_top=6,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=12,
        )

        self.box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=12)

        self.label = Gtk.Label(
            halign=Gtk.Align.START, label=f"<b>{label}</b>", use_markup=True
        )

        self.box.pack_start(self.label, False, False, 0)

        if help_text is not None and len(help_text) > 0:
            self.image = Gtk.Image(
                icon_name="dialog-question",
                icon_size=Gtk.IconSize.SMALL_TOOLBAR,
                tooltip_text=help_text,
            )

            self.box.pack_start(self.image, False, False, 0)

        self.pack_start(self.box, True, True, 0)
        self.pack_end(widget, expand, expand, 0)


class ScoreBox(Gtk.Box):
    """Bunch of buttons to select the score of a game

    See Also
    --------
    gi.repository.Gtk.Box
    """

    def __init__(self, score, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        score : int
            The current score to show in this widget
        """

        super().__init__(*args, **kwargs)

        self.score = score
        self.buttons = list()

        for index in range(0, 6):
            icon_name = "non-starred-symbolic"
            if index == 0:
                icon_name = "list-remove-symbolic"
            elif index <= self.score:
                icon_name = "starred-symbolic"

            button = Gtk.Button(
                image=Gtk.Image(
                    icon_name=icon_name, icon_size=Gtk.IconSize.BUTTON
                ),
            )
            button.connect("clicked", self.on_activate_button)

            # Quick access to the index value of the button
            setattr(button, "index", index)

            self.pack_start(button, False, False, 0)
            self.buttons.append(button)

        add_style_class_to_widget(Gtk.STYLE_CLASS_LINKED, self)

    def on_activate_button(self, button, **kwargs):
        """Update the image with the new selected score

        Parameters
        ----------
        button : gi.repository.Gtk.Button
            The button which have been clicked
        """

        self.score = button.index

        for index in range(1, 6):
            self.buttons[index].get_image().set_from_icon_name(
                (
                    "starred-symbolic"
                    if self.score >= index
                    else "non-starred-symbolic"
                ),
                Gtk.IconSize.BUTTON,
            )
