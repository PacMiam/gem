# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gtk


class InfoBar(Gtk.InfoBar):
    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemInterface
            The instance of the main interface
        """

        super().__init__(**kwargs)

        self.interface = interface

        self.icon = Gtk.Image(
            icon_name="dialog-information-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            margin_start=6,
        )

        self.label = Gtk.Label(label="infobar", wrap=True)

        box = self.get_content_area()
        box.add(self.icon)
        box.add(self.label)

    def do_response(self, response_id: int):
        """Manage the response signal emits by the buttons

        Parameters
        ----------
        response_id : int
            The identifier of the response
        """

        # Hide the infobar when the close button was clicked
        if response_id == Gtk.ResponseType.CLOSE:
            self.set_revealed(False)

    def set_informational_message(
        self, message: str, message_type: Gtk.MessageType
    ):
        """Set a new message in the infobar

        Parameters
        ----------
        message : str
            The text to insert in the infobar
        message_type : gi.repository.Gtk.MessageType
            The type of the informational message
        """

        match message_type:
            case Gtk.MessageType.ERROR:
                icon_name = "dialog-error-symbolic"
            case Gtk.MessageType.INFO:
                icon_name = "dialog-information-symbolic"
            case Gtk.MessageType.QUESTION:
                icon_name = "dialog-question-symbolic"
            case Gtk.MessageType.WARNING:
                icon_name = "dialog-warning-symbolic"
            case _:
                icon_name = "action-unavailable-symbolic"

        self.icon.set_from_icon_name(icon_name, Gtk.IconSize.LARGE_TOOLBAR)
        self.label.set_label(message)

        self.set_message_type(message_type)
