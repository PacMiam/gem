# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _
from pathlib import Path
from typing import Self, TYPE_CHECKING

from gi.repository import GdkPixbuf, Gtk, Pango

from geode_gem.configuration import GamesColumns
from geode_gem.interface.utils import replace_for_markup

if TYPE_CHECKING:
    from geode_gem.core.collection import GenericCollection


class ListBox(Gtk.ListBox):
    """Represent a generic ListBox for both game and collection storages

    Attributes
    ----------
    pixbuf : gi.repository.GdkPixbuf.Pixbuf
        The cache of the pixbuf used to represent the listbox
    rows_cache : dict
        The cache storage of the appened rows

    See Also
    --------
    gi.repository.Gtk.ListBox
    """

    TYPE = "generic"

    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemWindow
            The instance of the window linked to the main application
        """

        super().__init__(**kwargs)

        self.interface = interface
        self.pixbuf = None
        self.rows_cache = dict()

    def add(self, row):
        """Store the new row in the cache storage before adding in the listbox

        Parameters
        ----------
        row : gi.repository.Gtk.ListBoxRow
            The instance of the row to add in the listbox

        See Also
        --------
        gi.repository.Gtk.ListBoxRow.add
        """

        if hasattr(row, "geode_item"):
            self.rows_cache[row.geode_item.identifier] = row

        super().add(row)

    @property
    def for_type(self):
        """Retrieve the type of the entities stored in the listbox

        Returns
        -------
        str
            The row entity type
        """

        return self.TYPE

    def generate_pixbuf_icon_from_collection(
        self: Self,
        collection: "GenericCollection",
        icon_size: Gtk.IconSize = Gtk.IconSize.DND,
    ) -> GdkPixbuf.Pixbuf:
        """Generate the Pixbuf instance for the specified collection icon

        Parameters
        ----------
        collection : geode_gem.core.collection.GenericCollection
            The instance of the collection
        icon_size : gi.repository.Gtk.IconSize
            The size of the pixbuf to generate

        Returns
        -------
        GdkPixbuf.Pixbuf
            The Pixbuf instance of the collection icon
        """

        valid, width, height = Gtk.IconSize.lookup(icon_size)
        if not valid:
            return

        icon = collection.icon
        if icon is None:
            icon = "image-missing-symbolic"

        try:
            icon_path = Path(icon).expanduser()
            if icon_path.exists() and icon_path.is_file():
                return GdkPixbuf.Pixbuf.new_from_file_at_size(
                    icon, width, height
                )

            information = self.interface.icons_theme.lookup_icon(
                icon, min(width, height), Gtk.IconLookupFlags.FORCE_SIZE
            )
            if information is not None:
                return information.load_icon()

        except Exception:
            pass

        return self.interface.icons_blank[min(width, height)]

    def invalidate(self, *args):
        """Invalidate the listbox"""

        self.invalidate_sort()
        self.invalidate_filter()

    def reset(self):
        """Clear the listbox"""

        self.pixbuf = None

        self.rows_cache.clear()

        for child in self.get_children():
            self.remove(child)


class ListBoxRow(Gtk.ListBoxRow):
    """Represent a generic ListBoxRow

    Attributes
    ----------
    geode_item : geode_gem.core.entity.GeodeGemEntity
        The core entity related to the listbox row

    See Also
    --------
    gi.repository.Gtk.ListBoxRow
    """

    def __init__(self, item, *args, grid=Gtk.Box, **kwargs):
        """Constructor

        Parameters
        ----------
        item : geode_gem.core.entity.GeodeGemEntity
            The entity instance to associate with the current row
        """

        super().__init__(*args, **kwargs)

        self.geode_item = item

        match grid:
            case Gtk.Grid:
                self.grid = grid(border_width=4, column_spacing=8)
            case _:
                self.grid = grid(border_width=4, spacing=8)

        self.add(self.grid)


class ListBoxCollections(ListBox):
    """Contains all the collection"""

    TYPE = "collections"

    def add_row(self, collection):
        """Append the specified collection as a new row in the listbox

        Parameters
        ----------
        collection : geode_gem.core.collection.Collection
            The instance of the collection to append in the listbox
        """

        row = ListBoxRow(collection, grid=Gtk.Grid)

        icon = Gtk.Image(
            pixbuf=self.generate_pixbuf_icon_from_collection(collection)
        )

        title = Gtk.Label(
            label=f"<b>{collection.label}</b>",
            ellipsize=Pango.EllipsizeMode.END,
            use_markup=True,
            xalign=0,
        )

        games = len(collection)
        message = f"{games} game" if games < 2 else f"{games} games"

        subtitle = Gtk.Label(
            label=f"<small>{message}</small>",
            ellipsize=Pango.EllipsizeMode.END,
            use_markup=True,
            xalign=0,
        )

        context = subtitle.get_style_context()
        context.add_class(Gtk.STYLE_CLASS_DIM_LABEL)

        row.grid.attach(icon, 0, 0, 1, 2)
        row.grid.attach(title, 1, 0, 1, 1)
        row.grid.attach(subtitle, 1, 1, 1, 1)
        row.show_all()

        self.add(row)

    def connect_widgets(self):
        """Connect signals to the internal widgets"""

        self.connect("row-activated", self.interface.on_select_collection)

        self.set_filter_func(self.on_filter_collections)
        self.set_header_func(self.on_header_collections)
        self.set_sort_func(self.on_sort_collections)

    def on_filter_collections(self, row, *args):
        """Filter list with consoles searchentry text

        Parameters
        ----------
        row : Gtk.ListBoxRow
            The row which have been activated
        """

        hide = self.interface.actions["hide_collections"].get_state()

        if hide.get_boolean() and len(row.geode_item) == 0:
            return False

        widget = self.interface.toolbar_collections.search_entry
        if widget is None:
            return False

        filter_text = widget.get_text().strip().lower()
        if not len(filter_text):
            return True

        return filter_text in row.geode_item.label.lower()

    def on_header_collections(self, row, before, *args):
        """Update collections row header based on the favorite status

        Parameters
        ----------
        row : Gtk.ListBoxRow
            The row to update
        before : Gtk.ListBoxRow
            The row before the specified one, None if this one if the first
        """

        header = None

        if before is None and row.geode_item.favorite:
            header = Gtk.Label(label=f"<b>{_('Favorites')}</b>")

        elif not row.geode_item.favorite and (
            not before or before.geode_item.favorite
        ):
            header = Gtk.Label(label=f"<b>{_('Collections')}</b>")

        if header is not None:
            header.set_use_markup(True)
            header.set_margin_top(6)
            header.set_margin_bottom(6)

            context = header.get_style_context()
            context.add_class(Gtk.STYLE_CLASS_DIM_LABEL)

        row.set_header(header)

    def on_sort_collections(self, first, second, *args):
        """Sort collections to reorganize them

        Parameters
        ----------
        first : Gtk.ListBoxRow
            First row to compare with the second
        second : Gtk.ListBoxRow
            Second row to compare with the first
        """

        if not first.geode_item.favorite == second.geode_item.favorite:
            return first.geode_item.favorite < second.geode_item.favorite

        return first.geode_item.label.lower() > second.geode_item.label.lower()


class ListBoxGames(ListBox):
    """Contains all the games from a dedicated collection"""

    TYPE = "games"

    def __init__(self, *args, **kwargs):
        """Constructor

        See Also
        --------
        geode_gem.interface.listbox.ListBox
        """

        super().__init__(*args, **kwargs)

        self.sorted_column = GamesColumns.LABEL
        self.sorted_order = Gtk.SortType.ASCENDING

    def add_row(self, game):
        """Append the specified game as a new row in the listbox

        Parameters
        ----------
        game : geode_gem.core.game.Game
            The instance of the game to append in the listbox
        """

        row = ListBoxRow(game)

        row.image_game_favorite = Gtk.Image(
            icon_name="emblem-favorite-symbolic" if game.favorite else "",
            icon_size=Gtk.IconSize.MENU,
        )
        row.image_game_icon = Gtk.Image(
            pixbuf=self.generate_pixbuf_icon_from_game(game)
        )
        row.label_game = Gtk.Label(
            label=replace_for_markup(game.label),
            ellipsize=Pango.EllipsizeMode.END,
            use_markup=True,
            xalign=0,
        )
        row.image_game_finished = Gtk.Image(
            icon_name="emblem-ok-symbolic" if game.finished else "",
            icon_size=Gtk.IconSize.MENU,
        )
        row.image_game_multiplayer = Gtk.Image(
            icon_name=(
                "system-users-symbolic"
                if game.multiplayer
                else "avatar-default-symbolic"
            ),
            icon_size=Gtk.IconSize.MENU,
        )

        row.grid.pack_start(row.image_game_favorite, False, False, 0)
        row.grid.pack_start(row.image_game_icon, False, False, 0)
        row.grid.pack_start(row.label_game, True, True, 0)
        row.grid.pack_start(row.image_game_finished, False, False, 0)
        row.grid.pack_start(row.image_game_multiplayer, False, False, 0)
        row.show_all()

        self.add(row)

    def connect_widgets(self, signals):
        """Connect signals to the internal widgets

        Parameters
        ----------
        signals : dict
            The current storage of signals from the main application

        Returns
        -------
        dict
            The updated storage of signals
        """

        self.set_filter_func(self.on_filter_games)
        self.set_sort_func(self.on_sort_games)

        signals[self] = [
            self.connect("row-selected", self.interface.on_select_game),
        ]

        return signals

    def do_row_activated(self, row):
        """Activate the selected game to start a new launcher process

        Parameters
        ----------
        path : gi.repository.Gtk.TreePath
            The path of the activated row
        column : gi.repository.Gtk.TreeViewColumn
            The column which have been activated
        """

        game = row.geode_item
        if (
            game is not None
            and not self.interface.application.check_game_is_started(game)
        ):
            self.interface.application.emit("start-game", game)

    def generate_pixbuf_icon_from_game(self, game, icon_size=Gtk.IconSize.MENU):
        """Generate the Pixbuf instance for the specified game icon

        Notes
        -----
        When the game have no custom icon or the file path do not exists, the
        collection icon will be retrieved as fallbak through the self.pixbuf
        variable.

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game

        Returns
        -------
        GdkPixbuf.Pixbuf
            The Pixbuf instance of the game icon
        """

        valid, width, height = Gtk.IconSize.lookup(icon_size)
        if not valid:
            return

        if self.pixbuf is None:
            self.pixbuf = self.generate_pixbuf_icon_from_collection(
                game.collection, icon_size=icon_size
            )

        # Retrieve the collection icon when the game have no custom icon
        if game.icon is None:
            return self.pixbuf

        try:
            path = Path(game.icon).expanduser()

            # Check if the game icon is a file path
            if path.exists() and path.is_file():
                icon = self.interface.get_pixbuf_from_cache(
                    min(width, height), game.checksum
                )
                if icon is None:
                    return self.interface.add_image_in_cache(
                        min(width, height), path, game.checksum
                    )

                return icon

            # Check if the game icon is an icon name
            information = self.interface.icons_theme.lookup_icon(
                game.icon, min(width, height), Gtk.IconLookupFlags.FORCE_SIZE
            )
            if information is not None:
                return information.load_icon()

        except Exception:
            pass

        return self.pixbuf

    def on_filter_games(self, row, *args):
        """Filter games list based on the filter menu states

        Parameters
        ----------
        row : Gtk.ListBoxRow
            The row which have been activated
        """

        game = row.geode_item

        data = {
            "filter_favorite": game.favorite,
            "filter_finished": game.finished,
            "filter_multiplayer": game.multiplayer,
            "filter_singleplayer": not game.multiplayer,
            "filter_unfavorite": not game.favorite,
            "filter_unfinished": not game.finished,
        }

        is_visible = True
        for key, value in data.items():
            if not self.interface.actions[key].get_state().get_boolean():
                is_visible &= not value

        filter_text = self.interface.get_filter_text_from_game_toolbar_entry()
        if len(filter_text) > 0:
            is_visible &= filter_text.lower() in game.label.lower()

        return is_visible

    def on_sort_games(self, first, second, *args):
        """Sort games to reorganize them

        Parameters
        ----------
        first : Gtk.ListBoxRow
            First row to compare with the second
        second : Gtk.ListBoxRow
            Second row to compare with the first
        """

        first = first.geode_item
        second = second.geode_item

        match self.sorted_column:
            case GamesColumns.FAVORITE:
                if not first.favorite == second.favorite:
                    if self.sorted_order == Gtk.SortType.ASCENDING:
                        return first.favorite > second.favorite
                    return first.favorite < second.favorite

            case GamesColumns.FINISHED:
                if not first.finished == second.finished:
                    if self.sorted_order == Gtk.SortType.ASCENDING:
                        return first.finished > second.finished
                    return first.finished < second.finished

            case GamesColumns.MULTIPLAYER:
                if not first.multiplayer == second.multiplayer:
                    if self.sorted_order == Gtk.SortType.ASCENDING:
                        return first.multiplayer > second.multiplayer
                    return first.multiplayer < second.multiplayer

        if self.sorted_order == Gtk.SortType.ASCENDING:
            return first.label.lower() < second.label.lower()
        return first.label.lower() > second.label.lower()

    def set_sorted_column(self, sort_column):
        """Define the sorted column

        Parameters
        ----------
        sort_column : geode_gem.configuration.GamesColumns
            The enum object related to the user configuration choice
        """

        self.sorted_column = sort_column

        if len(self.rows_cache) > 0:
            self.invalidate()

    def set_sorted_order(self, sort_order):
        """Define the sorted order

        Parameters
        ----------
        sort_order : gi.repository.Gtk.SortType
            The enum object related to the user configuration choice
        """

        self.sorted_order = sort_order

        if len(self.rows_cache) > 0:
            self.invalidate()

    def update_game(self, game):
        """Update the information for the specified game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to retrieve information
        """

        if game.identifier not in self.rows_cache:
            return

        row = self.rows_cache[game.identifier]

        row.image_game_favorite.set_from_icon_name(
            "emblem-favorite-symbolic" if game.favorite else "",
            Gtk.IconSize.MENU,
        )
        row.image_game_icon.set_from_pixbuf(
            self.generate_pixbuf_icon_from_game(game)
        )
        row.label_game.set_label(game.label)
        row.image_game_finished.set_from_icon_name(
            "emblem-ok-symbolic" if game.finished else "",
            Gtk.IconSize.MENU,
        )
        row.image_game_multiplayer.set_from_icon_name(
            (
                "system-users-symbolic"
                if game.multiplayer
                else "avatar-default-symbolic"
            ),
            Gtk.IconSize.MENU,
        )
