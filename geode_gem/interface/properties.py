# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import Gtk

from geode_gem.interface.utils import (
    add_style_class_to_widget,
    get_pixbuf_from_name,
    get_path_from_string,
)
from geode_gem.interface.widgets import FramedListBox, ListBoxOption


class GeodeGemGameProperties(Gtk.Dialog):
    """Configure the selected game

    Attributes
    ----------
    game : geode_gem.core.game.GenericGame
        The instance of the selected game
    interface : geode_gem.interface.GeodeGemInterface
        The instance of the parent window

    See Also
    --------
    gi.repository.Gtk.Dialog
    """

    def __init__(self, interface, game, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemWindow
            The instance of the window linked to the main application
        game : geode_gem.core.game.GenericGame
            The currently selected game

        See Also
        --------
        gi.repository.Gtk.Dialog
        """

        super().__init__(
            destroy_with_parent=True,
            transient_for=interface,
            use_header_bar=True,
            **kwargs,
        )

        self.game = game
        self.icon = self.game.icon

        self.interface = interface

        self.__init_widgets__()
        self.__packing_widgets__()
        self.__connect_widgets__()

        self.set_header_icon_from_name(self.icon)

        self.show_all()

    def __init_widgets__(self):
        """Initialize the interface widgets"""

        self.headerbar = self.get_header_bar()
        self.headerbar.set_title(self.get_title())

        self.image_game_icon = Gtk.Image()

        self.button_apply = Gtk.Button(label=_("Accept"))
        add_style_class_to_widget(
            Gtk.STYLE_CLASS_SUGGESTED_ACTION, self.button_apply
        )

        self.notebook = Gtk.Notebook(
            show_border=False, show_tabs=True, tab_pos=Gtk.PositionType.LEFT
        )

        self.notebook_tab_general = NotebookTabGeneral(self)
        self.notebook_label_general = Gtk.Label(
            label="<b>%s</b>" % _("General"), use_markup=True
        )

    def __connect_widgets__(self):
        """Connect Gtk signal to the dialog widgets"""

        self.notebook_tab_general.connect_widgets()

        self.button_apply.connect("clicked", self.on_apply_modification)

    def __packing_widgets__(self):
        """Packing together the interface widgets"""

        self.headerbar.pack_start(self.image_game_icon)
        self.headerbar.pack_end(self.button_apply)

        self.grid = self.get_content_area()
        self.grid.pack_start(self.notebook, True, True, 0)

        self.notebook.append_page(
            self.notebook_tab_general, self.notebook_label_general
        )

        self.notebook_tab_general.packing_widgets()

    @property
    def data(self) -> dict:
        """Retrieve the game properties from all the tabs

        Returns
        -------
        The game properties as dictionary
        """

        data = dict()
        for tab in (self.notebook_tab_general,):
            data[tab.SECTION] = tab.data

        return data

    def on_apply_modification(self, *args, **kwargs):
        """Emit the response type Apply when the user validate the dialog"""

        self.response(Gtk.ResponseType.APPLY)

    def set_header_icon_from_name(self, icon_name):
        """Update the icon pixbuf in the headerbar

        Parameters
        ----------
        icon_name : str
            The name of the icon to render in the headerbar
        """

        if icon_name is None or len(icon_name) == 0:
            icon_name = None

        self.image_game_icon.set_from_pixbuf(
            get_pixbuf_from_name(
                self.interface.icons_theme,
                icon_name,
                fallback_name=self.game.collection.icon,
                size=32,
            )
        )


class NotebookTabGeneral(Gtk.ScrolledWindow):
    """Represents the common options for all the games

    See Also
    --------
    gi.repository.Gtk.ScrolledWindow
    """

    SECTION = "general"

    def __init__(self, parent, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        parent : geode_gem.interface.GeodeGemInterface
            The main instance of the application
        """

        super().__init__(*args, **kwargs)

        self.parent = parent

        self.grid = Gtk.Box(
            margin_bottom=12,
            margin_end=12,
            margin_start=12,
            margin_top=12,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=18,
        )

        self.frame_metadata = FramedListBox()
        self.frame_flags = FramedListBox()

        self.file_filter_metadata_icon = Gtk.FileFilter()
        self.file_filter_metadata_icon.add_pixbuf_formats()

        self.file_chooser_metadata_icon = Gtk.FileChooserDialog(
            action=Gtk.FileChooserAction.OPEN,
            destroy_with_parent=True,
            filter=self.file_filter_metadata_icon,
            local_only=True,
            title=_("Select a custom icon"),
            transient_for=parent,
            use_header_bar=True,
        )
        self.file_chooser_metadata_icon.add_buttons(
            _("Cancel"),
            Gtk.ResponseType.CANCEL,
            _("Accept"),
            Gtk.ResponseType.ACCEPT,
        )

        self.label_metadata = Gtk.Label(
            label="<b>%s</b>" % _("Metadata"), use_markup=True
        )
        self.search_entry_metadata_label = Gtk.SearchEntry(
            placeholder_text=(
                parent.game.path.stem
                if parent.game.path is not None
                else parent.game.identifier
            ),
            primary_icon_pixbuf=None,
            text=parent.game.label,
        )
        self.listbox_metadata_label = ListBoxOption(
            _("Label"), self.search_entry_metadata_label
        )

        self.box_metadata_icon = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL, spacing=0
        )
        self.search_entry_metadata_icon = Gtk.SearchEntry(
            primary_icon_pixbuf=None,
            text=str(parent.game.icon) if parent.game.icon is not None else "",
        )
        self.button_metadata_icon = Gtk.Button(
            image=Gtk.Image(
                icon_name="document-open-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            ),
        )
        self.listbox_metadata_icon = ListBoxOption(
            _("Icon"),
            self.box_metadata_icon,
            help_text=_(
                "The icon can be retrieved from an absolute path or by "
                "finding the string in the icons theme"
            ),
        )

        self.label_flags = Gtk.Label(
            label="<b>%s</b>" % _("Flags"), use_markup=True
        )
        self.switch_flags_favorite = Gtk.Switch(
            active=parent.game.favorite, halign=Gtk.Align.END
        )
        self.listbox_flags_favorite = ListBoxOption(
            _("Favorite"), self.switch_flags_favorite
        )
        self.switch_flags_multiplayer = Gtk.Switch(
            active=parent.game.multiplayer, halign=Gtk.Align.END
        )
        self.listbox_flags_multiplayer = ListBoxOption(
            _("Multiplayer"), self.switch_flags_multiplayer
        )
        self.switch_flags_finished = Gtk.Switch(
            active=parent.game.finished, halign=Gtk.Align.END
        )
        self.listbox_flags_finished = ListBoxOption(
            _("Finished"), self.switch_flags_finished
        )

    def packing_widgets(self):
        """Packing together the dialog widgets"""

        self.grid.pack_start(self.label_metadata, False, False, 0)
        self.grid.pack_start(self.frame_metadata, False, False, 0)
        self.grid.pack_start(self.label_flags, False, False, 0)
        self.grid.pack_start(self.frame_flags, False, False, 0)

        self.frame_metadata.listbox.add(self.listbox_metadata_label)
        self.frame_metadata.listbox.add(self.listbox_metadata_icon)

        self.box_metadata_icon.pack_start(
            self.search_entry_metadata_icon, True, True, 0
        )
        self.box_metadata_icon.pack_start(
            self.button_metadata_icon, False, False, 0
        )
        add_style_class_to_widget(
            Gtk.STYLE_CLASS_LINKED, self.box_metadata_icon
        )

        self.frame_flags.listbox.add(self.listbox_flags_favorite)
        self.frame_flags.listbox.add(self.listbox_flags_multiplayer)
        self.frame_flags.listbox.add(self.listbox_flags_finished)

        self.add(self.grid)

    def connect_widgets(self):
        """Connect Gtk signal to the inner widgets"""

        self.button_metadata_icon.connect("clicked", self.do_select_icon_file)

        self.search_entry_metadata_icon.connect(
            "changed", self.on_update_icon_entry
        )

    @property
    def data(self) -> dict:
        """Retrieve the game properties from the tab general

        Returns
        -------
        dict
            The properties related to the general tab as a dictionary
        """

        return {
            "label": self.search_entry_metadata_label.get_text().strip(),
            "icon": self.search_entry_metadata_icon.get_text().strip(),
            "favorite": self.switch_flags_favorite.get_state(),
            "finished": self.switch_flags_finished.get_state(),
            "multiplayer": self.switch_flags_multiplayer.get_state(),
        }

    def do_select_icon_file(self, *args, **kwargs):
        """Open a file chooser dialog to select an icon file for the game

        See Also
        --------
        gi.repository.Gtk.FileChooserDialog
        """

        self.set_sensitive(False)

        path = get_path_from_string(
            self.search_entry_metadata_icon.get_text().strip()
        )

        if path.is_file():
            self.file_chooser_metadata_icon.set_filename(str(path))
        else:
            self.file_chooser_metadata_icon.set_current_folder(str(path))

        if self.file_chooser_metadata_icon.run() == Gtk.ResponseType.ACCEPT:
            filename = self.file_chooser_metadata_icon.get_filename()

            self.search_entry_metadata_icon.set_text(
                filename if filename is not None else ""
            )

        self.file_chooser_metadata_icon.hide()

        self.set_sensitive(True)

    def on_update_icon_entry(self, *args, **kwargs):
        """Received a changed event from the icon search entry

        Notes
        -----
        This event will update the icon in the headerbar to use the new one
        from the search entry

        See Also
        --------
        geode_gem.interface.properties.GeodeGemGameProperties
        """

        self.parent.set_header_icon_from_name(
            self.search_entry_metadata_icon.get_text().strip()
        )
