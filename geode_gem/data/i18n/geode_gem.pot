# File: geode_gem/interface/about.py, line: 43
msgid   "translator_credits"
msgstr  ""
#
# File: geode_gem/interface/about.py, line: 49
msgid   "Tested by"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 48
msgid   "Quit"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 51
msgid   "About..."
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 56
msgid   "Hide Empty Collections"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 63
msgid   "Horizontal"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 68
msgid   "Vertical"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 79
msgid   "Show sidebar"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 85
msgid   "Dark theme variant"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 89
msgid   "Collections"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 91
msgid   "Games"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 93
msgid   "Informative sidebar"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 104
msgid   "Main menu"
msgstr  ""
#
# File: geode_gem/interface/headerbar.py, line: 115
msgid   "View"
msgstr  ""
#
# File: geode_gem/interface/__init__.py, line: 346
msgid   "Game properties"
msgstr  ""
#
# File: geode_gem/interface/__init__.py, line: 599
msgid   "The game was stopped by raising a memory error"
msgstr  ""
#
# File: geode_gem/interface/__init__.py, line: 602
msgid   "There is a problem with this game. You can read the log output to "
        "see what is wrong."
msgstr  ""
#
# File: geode_gem/interface/__init__.py, line: 606
msgid   "An unknown error occurs during the process"
msgstr  ""
#
# File: geode_gem/interface/__init__.py, line: 609
#, python-format
msgid   "%(game_label)s: %(infobar_error_message)s"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 94
# File: geode_gem/interface/properties.py, line: 223
msgid   "Accept"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 105
msgid   "General"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 216
msgid   "Select a custom icon"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 221
msgid   "Cancel"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 228
msgid   "Metadata"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 236
# File: geode_gem/interface/toolbar.py, line: 122
msgid   "Label"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 253
msgid   "Icon"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 256
msgid   "The icon can be retrieved from an absolute path or by finding the "
        "string in the icons theme"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 262
msgid   "Flags"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 269
# File: geode_gem/interface/toolbar.py, line: 96
# File: geode_gem/interface/toolbar.py, line: 117
msgid   "Favorite"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 276
# File: geode_gem/interface/sidebar.py, line: 284
# File: geode_gem/interface/toolbar.py, line: 101
# File: geode_gem/interface/toolbar.py, line: 132
msgid   "Multiplayer"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 283
# File: geode_gem/interface/toolbar.py, line: 106
# File: geode_gem/interface/toolbar.py, line: 127
msgid   "Finished"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 287
msgid   "Misc"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 293
msgid   "Tags"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 295
msgid   "The tags must be separated with a comma"
msgstr  ""
#
# File: geode_gem/interface/properties.py, line: 299
# File: geode_gem/interface/sidebar.py, line: 130
msgid   "Score"
msgstr  ""
#
# File: geode_gem/interface/revealer.py, line: 44
msgid   "Games Loading in Progress..."
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 104
msgid   "Played"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 110
msgid   "Time Played"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 117
msgid   "Last Played"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 124
msgid   "Installation Date"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 290
# File: geode_gem/interface/toolbar.py, line: 104
msgid   "Singleplayer"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 324
#, python-format
msgid   "Screenshot taken the %x"
msgstr  ""
#
# File: geode_gem/interface/sidebar.py, line: 360
msgid   "Unkwown"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 49
# File: geode_gem/interface/toolbar.py, line: 192
msgid   "Filter..."
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 98
msgid   "Unfavorite"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 108
msgid   "To Finish"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 110
msgid   "Reset Filters"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 142
msgid   "Ascending"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 147
msgid   "Descending"
msgstr  ""
#
# File: geode_gem/interface/toolbar.py, line: 162
msgid   "Play"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 526
msgid   "Today"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 528
msgid   "Yesterday"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 530
msgid   "Tomorrow"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 532
#, python-format
msgid   "%d days ago"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 534
#, python-format
msgid   "In %d days"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 539
msgid   "Last month"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 541
msgid   "Next month"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 543
#, python-format
msgid   "%d months ago"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 545
#, python-format
msgid   "In %d months"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 550
msgid   "Last year"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 552
msgid   "Next year"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 554
#, python-format
msgid   "%d years ago"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 556
#, python-format
msgid   "In %d years"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 592
msgid   "1 second"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 592
#, python-format
msgid   "%d seconds"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 595
msgid   "1 minute"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 595
#, python-format
msgid   "%d minutes"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 597
msgid   "1 hour"
msgstr  ""
#
# File: geode_gem/interface/utils.py, line: 597
#, python-format
msgid   "%d hours"
msgstr  ""
