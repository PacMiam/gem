# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field
from logging import getLogger
from pathlib import Path
from typing import Any, NoReturn, Self, TYPE_CHECKING
import sqlite3

if TYPE_CHECKING:
    from geode_gem.core.game import GenericGame


def parse_where(where_conditions: tuple[str]) -> tuple[str, tuple[str]]:
    """Parse where conditions to use the DB-API’s parameter substitution

    Parameters
    ----------
    where_conditions : tuple
        SQL where conditions as tuple of string

    Returns
    -------
    tuple, tuple
        Parsed SQL Where statement as tuple of key and tuple of value

    Examples
    --------
    >>> parse_where(("name = foo", "id = 1"))
    ('name = ? AND id = ?'), ('foo', '1')
    >>> parse_where(("id = 42", "name like i?"))
    ('id = ? AND hash LIKE ?'), ('42', 'i?')
    """

    keys, values = [], []

    for element in where_conditions:
        try:
            key, symbol, value = element.split()
        except ValueError:
            raise ValueError(
                f"Spacing is mandatory to detect the symbols: '{element}'"
            )

        keys.append(f"{key.strip()} {symbol.upper()} ?")
        values.append(value.strip())

    return " AND ".join(keys), values


@dataclass
class Database:
    """Represents the Geode-Gem database model

    Attributes
    ----------
    database : str or pathlib.Path
        Sqlite database pathname
    arguments : dict
        Optional arguments sent to sqlite3.connect method
    connection : sqlite3.Connection, default: None
        Sqlite database connection instance
    schemata : dict
        The model of the Geode-Gem database

    See Also
    --------
    sqlite3
    """

    database: str | Path

    arguments: dict[str, Any] = field(default_factory=dict, init=False)
    connection: sqlite3.Connection | None = None

    schemata = {
        "games": [
            "hash TEXT PRIMARY KEY",
            "label TEXT NOT NULL DEFAULT ''",
            "icon TEXT NOT NULL DEFAULT ''",
            "favorite INTEGER NOT NULL DEFAULT 0",
            "finished INTEGER NOT NULL DEFAULT 0",
            "multiplayer INTEGER NOT NULL DEFAULT 0",
            "played INTEGER NOT NULL DEFAULT 0",
            "last_played TEXT NOT NULL DEFAULT ''",
            "last_played_time TEXT NOT NULL DEFAULT ''",
            "time_played TEXT NOT NULL DEFAULT ''",
            "environment BLOB DEFAULT NULL",
        ],
        "roms": [
            "id INTEGER PRIMARY KEY",
            "arguments TEXT NOT NULL DEFAULT ''",
            "emulator TEXT NOT NULL DEFAULT ''",
            "game_id TEXT NOT NULL DEFAULT ''",
            "hash TEXT NOT NULL",
            "FOREIGN KEY(hash) REFERENCES games(hash)",
        ],
    }

    def __post_init__(self: Self, **kwargs) -> NoReturn:
        """Constructor

        Parameters
        ----------
        database : pathlib.Path or str
            The Sqlite database path on filesystem. Use ':memory' to open a
            database that resides in RAM instead on filesystem.
        kwargs : dict
            Optional arguments send to the Sqlite connect method

        See Also
        --------
        sqlite3.connect

        Examples
        --------
        >>> SqliteDatabase("/tmp/foo.db")

        >>> SqliteDatabase(":memory:", isolation_level=None)
        """

        self.arguments = kwargs

    def __del__(self: Self) -> NoReturn:
        """Close database instance when object is deleted"""

        self.close()

    def close(self: Self) -> NoReturn:
        """Close the database connection

        See Also
        --------
        sqlite3.Connection.close
        """

        if self.connection is not None:
            self.connection.close()

            self.connection = None

    def check_schemata(self: Self) -> NoReturn:
        """Check if the database stored in Geode-Gem profile is valid

        Raises
        ------
        KeyError
            When a column is not valid in the stored database
        """

        logger = getLogger(__name__)

        for table, schema in self.schemata.items():
            logger.debug(f"Check schemata: look for the table {table}")
            columns = {
                element["name"]: element["type"]
                for element in self.table_info(table)
            }

            # The table do not exists in the database
            if len(columns) == 0:
                logger.debug(f"Create the table '{table}' in the database")
                self.create_table(table, *schema)
                continue

            schema_columns = {}
            schema_constraints = {}
            for element in schema:
                name, sqltype, *arguments = element.split(maxsplit=2)
                if name == "FOREIGN":
                    continue

                schema_columns[name] = sqltype
                schema_constraints[name] = " ".join(arguments)

            if not sorted(schema_columns) == sorted(columns):
                logger.warning(
                    "Check schemata: there is a difference in the schema for "
                    f"the table '{table}'"
                )

                # Unknown column
                for element in columns.keys() - schema_columns.keys():
                    logger.info(
                        f"Database migration: remove the column '{element}' "
                        f"from the table {table}"
                    )
                    self.alter_table_drop_column(table, element)

                # Missing column
                for element in schema_columns.keys() - columns.keys():
                    logger.info(
                        f"Database migration: add the column '{element}' in "
                        f"the table {table}"
                    )

                    self.alter_table_add_column(
                        table,
                        element,
                        (
                            f"{schema_columns[element]} "
                            f"{schema_constraints[element]}"
                        ),
                    )

    def connect(function):
        """Decorator which open a database connection for specified function

        See Also
        --------
        sqlite3.connect
        """

        def __function__(self, *args, **kwargs):
            if self.connection is None:
                self.connection = sqlite3.connect(
                    self.database, **self.arguments
                )

            return function(self, *args, **kwargs)

        return __function__

    @connect
    def backup(self: Self, backup_path: str | Path, **kwargs) -> NoReturn:
        """Backup the current Sqlite database to another location

        Parameters
        ----------
        backup_path : pathlib.Path or str
            The backup Sqlite database path

        See Also
        --------
        sqlite3.Connection.backup

        Examples
        --------
        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.backup("/tmp/bar.db")

        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.backup("/tmp/baz.db", pages=1, progress=lambda x: print(x))
        """

        with sqlite3.connect(backup_path) as backup_connection:
            self.connection.backup(backup_connection, **kwargs)

    @connect
    def commit(self: Self) -> NoReturn:
        """Commit the current transaction if necessary

        See Also
        --------
        sqlite3.Connection.commit
        sqlite3.Connection.in_transaction
        """

        if self.connection.in_transaction:
            self.connection.commit()

    @connect
    def execute(
        self: Self, request: str, args: str | None = None, commit: bool = True
    ) -> list[sqlite3.Row] | None:
        """Open a new connection and execute the specified SQL request

        Parameters
        ----------
        request : str
            SQL request to execute in a Sqlite database cursor
        args: str or None, default: None
            Arguments to use with the SQL request
        commit : bool, default: True
            Commit the current transaction if there is any changes

        Returns
        -------
        list[sqlite3.Row] or None
            A list of entries when the specified request returns some data,
            None otherwise

        See Also
        --------
        sqlite3.Connection.commit
        sqlite3.Connection.execute
        sqlite3.Cursor.fetchall

        Examples
        --------
        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.execute("SELECT * FROM foo;")
        [<sqlite3.Row object at 0x7fd8bc616c50>]
        """

        if sqlite3.complete_statement(request):
            # TODO: Allow to customize the row_factory
            self.connection.row_factory = sqlite3.Row

            # Detect the current SQL request scope
            request_type = request.split()[0].strip().upper()

            if isinstance(args, (list, tuple)):
                cursor = self.connection.execute(request, args)
            else:
                cursor = self.connection.execute(request)

            if request_type in ("SELECT", "PRAGMA"):
                return cursor.fetchall()

            if self.connection.in_transaction and commit:
                self.connection.commit()

    @connect
    def rollback(self: Self) -> NoReturn:
        """Rolls back any changes from the database since the last commit

        See Also
        --------
        sqlite3.Connection.in_transaction
        sqlite3.Connection.rollback
        """

        if self.connection.in_transaction:
            self.connection.rollback()

    def alter_table_add_column(
        self: Self, table: str, column: str, constraints: str | None = None
    ) -> NoReturn:
        """Add a new column to the specified table

        https://www.sqlite.org/lang_altertable.html

        Parameters
        ----------
        table : str
            The table name in current database
        column : str
            The new column name to add in the specified table
        constraints : str, default: None
            The column constraints used with the add_column argument

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table_add_column("foo", "bar")
        >>> db.alter_table_add_column("foo", "baz", constraints="TEXT")
        """

        request = f'ALTER TABLE "{table}" ADD COLUMN "{column}"'
        if isinstance(constraints, str):
            request += constraints

        self.execute(f"{request};")

    def alter_table_drop_column(
        self: Self, table: str, column: str
    ) -> NoReturn:
        """Drop a column from the specified table

        https://www.sqlite.org/lang_altertable.html

        Parameters
        ----------
        table : str
            The table name in current database
        column : str
            The column name to drop from the specified table

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table_drop_column("foo", "bar")
        """

        self.execute(f'ALTER TABLE "{table}" DROP COLUMN "{column}";')

    def alter_table_rename_column(
        self: Self, table: str, column: str, new_name: str
    ) -> NoReturn:
        """Rename a column from the specified table with a new name

        https://www.sqlite.org/lang_altertable.html

        Parameters
        ----------
        table : str
            The table name in current database
        column : str
            The current name of the column from the specified table
        new_name : str
            The new name for the column

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table("foo", "baz", "qux")
        """

        self.execute(
            f'ALTER TABLE "{table}" RENAME COLUMN "{column}" '
            f'TO "{new_name}";'
        )

    def alter_table_rename_table(
        self: Self, table: str, new_name: str
    ) -> NoReturn:
        """Rename the specified table with a new name

        https://www.sqlite.org/lang_altertable.html

        Parameters
        ----------
        table : str
            The table name in current database
        new_name : str
            The new table name

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table_rename_table("foo", "bar")
        """

        self.execute(f'ALTER TABLE "{table}" RENAME TO "{new_name}";')

    def create_table(self: Self, table: str, *args) -> NoReturn:
        """Create a table in current database with a specific schema

        https://www.sqlite.org/lang_createtable.html

        Parameters
        ----------
        table : str
            The table name in current database
        args : list
            The schema as a list of string

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.create_table("foo", "id integer", "name text")
        """

        self.execute(f"CREATE TABLE \"{table}\" ({', '.join(args)});")

    def delete(
        self: Self, table: str, where: list[str] | None = None
    ) -> NoReturn:
        """Delete a subset of data from a specific table

        https://www.sqlite.org/lang_delete.html

        Parameters
        ----------
        table : str
            The table name in current database
        where : list or tuple, default: None
            The where statement structure used to retrieve specific values

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.delete("foo")

        >>> db = SqliteDatabase(":memory:")
        >>> db.delete("foo", where=["name = bar"])
        """

        request = f"DELETE FROM {table}"

        if where is not None and len(where) > 0:
            keys, values = parse_where(where)
            self.execute(f"{request} WHERE {keys};", values)
        else:
            self.execute(f"{request};")

    def drop_table(self: Self, table: str, if_exists: bool = True) -> NoReturn:
        """Remove a specific table from current database

        https://www.sqlite.org/lang_droptable.html

        Parameters
        ----------
        table : str
            The table name in current database
        if_exists : bool, default: True
            If True, disable the transaction error when the table already exists

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.drop_table("foo")
        """

        self.execute(f"DROP TABLE {'IF EXISTS ' if if_exists else ''}{table};")

    def get_data_from_game(self: Self, game: "GenericGame") -> dict[str, Any]:
        """Retrieve rows from the database for a specific game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game for which the data will be retrieved

        Returns
        -------
        dict
            The fetched information for the specified instance of the game
        """

        data = {}

        game_row = self.select("games", "*", where=[f"hash = {game.checksum}"])
        if len(game_row) == 0:
            return data

        for key in game_row[0].keys():
            data[key] = game_row[0][key]

        if isinstance(game.DATABASE_TABLE, str):
            # TODO: Remove the useless id value
            additional_row = self.select(
                game.DATABASE_TABLE, "*", where=[f"hash = {game.checksum}"]
            )
            if len(additional_row) == 0:
                return data

            for key in additional_row[0].keys():
                data[key] = additional_row[0][key]

        return data

    def insert(
        self: Self, table: str, data: dict[str, Any], commit: bool = True
    ) -> NoReturn:
        """Insert a new data in a specific table

        https://www.sqlite.org/lang_insert.html

        Parameters
        ----------
        table : str
            The table name in current database
        data : dict
            The data to insert with table column as key
        commit : bool, default: True
            If True, commit the current transaction

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.insert("foo", {"id": 10, "name": "alice"})

        >>> db = SqliteDatabase(":memory:")
        >>> db.insert("foo", {"id": 42, "name": "bob"}, commit=False)
        >>> db.rollback()
        """

        keys = ", ".join(data.keys())
        values = ", ".join("?" * len(data))

        self.execute(
            f"INSERT INTO {table}({keys}) VALUES({values});",
            list(data.values()),
            commit=commit,
        )

    def save_game(
        self: Self, game: "GenericGame", commit: bool = True
    ) -> NoReturn:
        """Store the specified game into the current database

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
           The instance of the game which will be stored in the database
        commit : bool, default: True
           Run the commit action in the database
        """

        for table, data in game.as_database_row().items():
            row = self.select(table, "*", where=[f"hash = {game.checksum}"])

            if len(row) == 0:
                data["hash"] = game.checksum
                self.insert(table, data, commit=commit)
            else:
                self.update(
                    table,
                    data,
                    where=[f"hash = {game.checksum}"],
                    commit=commit,
                )

    def select(
        self: Self, table: str, *args, where: list[str] | None = None
    ) -> list[sqlite3.Row]:
        """Retrieve a subset of data from a specific table

        https://www.sqlite.org/lang_select.html

        Parameters
        ----------
        table : str
            The table name in current database
        where : list or tuple, default: None
            The where statement structure used to retrieve specific values

        Returns
        -------
        list
            The request result as a list of sqlite3.Row

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.select("foo", '*')
        [<sqlite3.Row object at 0x865de2fcb3ad>,
         <sqlite3.Row object at 0x48def62cbda3>]

        >>> db = SqliteDatabase(":memory:")
        >>> db.select("foo", '*', where=["name = baz"])
        [<sqlite3.Row object at 0x865de2fcb3ad>]
        """

        request = f"SELECT {', '.join(*args)} FROM {table}"

        if where is not None and len(where) > 0:
            keys, values = parse_where(where)
            return self.execute(f"{request} WHERE {keys};", values)

        return self.execute(f"{request};")

    def table_info(self: Self, table: str) -> list[sqlite3.Row]:
        """Retrieve column information for the specified table

        https://www.sqlite.org/pragma.html#pragma_table_info

        Parameters
        ----------
        table : str
            The table name in current database

        Returns
        -------
        list
            The request result as a list of sqlite3.Row

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.table_info("foo")
        [<sqlite3.Row object at 0x145dfb367eda>]
        """

        return self.execute(f'PRAGMA table_info("{table}");')

    def update(
        self: Self,
        table: str,
        data: dict[str, Any],
        where: list[str] | None = None,
        commit: bool = True,
    ) -> NoReturn:
        """Modify a subset of data from a specific table

        https://www.sqlite.org/lang_update.html

        Parameters
        ----------
        table : str
            The table name in current database
        data : dict
            The new values to set with table column as key
        where : list or tuple
            The where statement structure used to retrieve specific values
        commit : bool, default: True
            If True, commit the current transaction

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.update("foo", {"name": "bar"}, where=["name = baz"])
        """

        set_keys = ", ".join([f"{key} = ?" for key in data])
        values = list(data.values())

        request = f"UPDATE {table} SET {set_keys}"

        if where is not None and len(where) > 0:
            where_keys, where_values = parse_where(where)
            values.extend(where_values)
            request += f"WHERE {where_keys}"

        self.execute(f"{request};", values, commit=commit)

    @property
    def version(self: Self) -> str:
        """Retrieve the version number of the run-time SQLite library

        Returns
        -------
        str
            The current version of the SQLite library

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.version
        3.37.0
        """

        return sqlite3.sqlite_version
