# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field, KW_ONLY
from typing import Any, NoReturn, Self

from geode_gem.configuration import Configuration
from geode_gem.core.utils import cast_str_to_type


@dataclass
class Entity:
    """Basic class to hold the common methods and attributes for common entities

    Attributes
    ----------
    identifier : str
        The identifier used to make the entity unique
    icon : str
        The file path or the icon name used to represent the entity
    label : str
        The human-readable string used to represent the entity
    environment : dict[str, str]
        Environment variables storage as dictionary structure (case sensitive)
    """

    identifier: str
    _: KW_ONLY
    icon: str = ""
    label: str = ""
    environment: dict[str, str] = field(default_factory=dict)

    def __setattr__(self: Self, name: str, value: Any) -> NoReturn:
        """Assign an attribute with a specific value

        Parameters
        ----------
        name : str
            The name of the attribute to assign in this class
        value : Any
            The assigned value to the class attribute
        """

        value = cast_str_to_type(value, self.__dataclass_fields__[name].type)
        super().__setattr__(name, value)

    def __str__(self: Self) -> str:
        """Returns the identifier of the entity when casted as a string

        Notes
        -----
        Adding this method will help to cast the entity instance as a string
        without leaking a lot of information (like the content of the array
        or dict storage).

        Returns
        -------
        str
            The identifier of the entity instance
        """

        return self.identifier

    def __post_init__(self: Self) -> NoReturn:
        """Initialize the optional attributes"""

        for attribute in ("icon", "label"):
            value = getattr(self, attribute)
            if value is None or (
                isinstance(value, str) and len(value.strip()) == 0
            ):
                setattr(self, attribute, self.identifier)

    def getenv(self: Self, key: str, default: str | None = None) -> str:
        """Retrieve the value of the specified environment variable

        Parameters
        ----------
        key : str
            The name of the environment variable
        default : str
            The fallback value of the environment variable if not available

        Returns
        -------
        str
            The value casted as a string of the environment variable
        """

        return self.environment.get(key, default)

    def putenv(self: Self, key: str, value: str) -> NoReturn:
        """Define an environment variable with a specified value

        Parameters
        ----------
        key : str
            The name of the environment variable
        value : str
            The value to set in the specified environment variable
        """

        self.environment[key] = str(value)

    def unsetenv(self: Self, key: str, ignore_missing: bool = False) -> None:
        """Unset the specified environment variable

        Parameters
        ----------
        key : str
            The name of the environment variable
        ignore_missing : bool
            Do not raise a KeyError if the environment variable do not exits
        """

        if ignore_missing and key not in self.environment:
            return

        del self.environment[key]

    @staticmethod
    def parse_configuration(parser: Configuration) -> dict[str, Any]:
        """Method used for defined the attributes from inheritance

        Parameters
        ----------
        parser : geode_gem.configuration.Configuration
            The instance of the configuration parser used to retrieve metadata
        """

        identifier = parser.get("metadata", "identifier")
        if identifier is not None and len(identifier.strip()) == 0:
            raise KeyError(
                "This identifier must be defined in the metadata section"
            )

        return {
            "identifier": identifier,
            "icon": parser.get("metadata", "icon", fallback=identifier),
            "label": parser.get("metadata", "label", fallback=identifier),
        }

    @classmethod
    def new_from_configuration(cls: Self, parser: Configuration) -> Self:
        """Generate a new entity from a configuration file parser

        Parameters
        ----------
        parser : geode_gem.configuration.Configuration
            The instance of the configuration parser used to retrieve metadata

        Returns
        -------
        geode_gem.core.entity.Entity
            The generated instance of the Entity class based on the
            configuration parser content
        """

        data = cls.parse_configuration(parser)

        instance = cls(**data)
        if parser.has_section("environment"):
            for option, value in parser.items("environment"):
                instance.putenv(option, value)

        return instance
