# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from configparser import ConfigParser
from dataclasses import dataclass
from pathlib import Path
from os import access, R_OK
from typing import NoReturn, Self


@dataclass
class Configuration(ConfigParser):
    """Advanced class to manage a configuration file with configparser

    Attributes
    ----------
    path : pathlib.Path or None
        The path of the file used to initialize the configuration parser

    See Also
    --------
    configparser.ConfigParser
    """

    path: Path | None = None

    def __post_init__(self: Self, **kwargs):
        """Check if the specified file can be used by the configuration parser

        Raises
        ------
        IsADirectoryError
            When the specified path is a directory
        """

        if self.path is not None and self.path.is_dir():
            raise IsADirectoryError(
                "Cannot use a directory to set the configuration instance"
            )

        super().__init__(**kwargs)

        # Keep the options case sensitive
        self.optionxform = str

        if (
            self.path is not None
            and self.path.exists()
            and access(self.path, R_OK)
        ):
            self.read(self.path)

    def getlist(self: Self, *args, separator: str = " ", **kwargs) -> list[str]:
        """Coerces the option as a list of string

        Parameters
        ----------
        separator : str
            The character used to split the value of the option

        Returns
        -------
        list
            The option as a list of string
        """

        return self.get(*args, **kwargs).split(separator)

    def getpath(self: Self, *args, **kwargs) -> Path:
        """Coerces the option as a Path object

        Returns
        -------
        pathlib.Path
            The option as a Path object
        """

        return Path(self.get(*args, **kwargs)).expanduser()

    def save(
        self: Self, path: Path | None = None, force_if_exists: bool = False
    ) -> NoReturn:
        """Save current configuration data on a specific file

        By default, the data will be saved in the file path specified in this
        class constructor

        Parameters
        ----------
        path : pathlib.Path or None, default: None
            File path used to save the configuration data
        force_if_exists : bool, default: False
            Force saving if the specified file does exists, otherwise an
            exception will be raised

        Raises
        ------
        FileExistsError
            When the specified path already exists and the force_if_exists
            parameter is set as False
        IsADirectoryError
            When the specified path (or inner file path) is a directory
        """

        if path is None:
            path = self.path

        if path.is_dir():
            raise IsADirectoryError(
                f"Cannot use the path '{path}' to save the configuration "
                "since its a directory"
            )

        if not force_if_exists and not self.path == path and path.exists():
            raise FileExistsError(
                f"Cannot save the configuration in the path '{path}' since "
                "this file already exists on the filesystem and the argument "
                "force_if_exists was not used"
            )

        with path.open("w") as fileobject:
            self.write(fileobject, space_around_delimiters=True)
