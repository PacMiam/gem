# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, time, timedelta
from importlib import resources
from os import access, environ, getenv, R_OK
from os.path import getctime
from pathlib import Path
from re import sub
from typing import Any, TYPE_CHECKING

if TYPE_CHECKING:
    from geode_gem.core.game import GenericGame


def are_equivalent_timestamps(first: int, second: int, delta: int = 0) -> bool:
    """Check if two timestamps are equivalent

    Parameters
    ----------
    first : int
        First timestamp
    second : int
        Second timestamp
    delta : int, optional
        Allowed difference between the two timestamps

    Returns
    -------
    bool
        True if timestamps are equivalent, False otherwise
    """

    return abs(int(float(first)) - int(float(second))) in range(0, delta + 1)


def cast_str_to_type(
    value: str, cast_type: type, list_separator: str = " "
) -> Any:
    """Cast a specific type from the specified string value

    Parameters
    ----------
    value : str
        The string to cast as the specified type
    cast_type : type
        The type used to cast the string
    list_separator : str
        The separator used to split the string into a list

    Returns
    -------
    Any
        The new value if the type was managed by this function, the original
        otherwise
    """

    if isinstance(value, str):
        if cast_type is bool:
            return value.lower() in ["y", "yes", "on", "1"]
        if cast_type is float:
            return float(value)
        if cast_type is int:
            return int(value)
        if cast_type is list:
            return [element.strip() for element in value.split(list_separator)]
        if cast_type is datetime:
            if len(value) == 0:
                return None
            return datetime.fromisoformat(value)
        if cast_type is time:
            if len(value) == 0:
                return None
            return time.fromisoformat(value)
        if cast_type is timedelta:
            if len(value) == 0:
                return None
            seconds, *_ = value.split(".")
            return timedelta(seconds=int(seconds))
        if cast_type is Path:
            if len(value) == 0:
                return None
            return Path(value).expanduser()

    return value


def check_path(path: Path, fallback_path: str | None = None) -> Path | None:
    """Check if the specified path exists

    Parameters
    ----------
    path : str or pathlib.Path
        The path of the file to check on the fileystem
    fallback_path : str or pathlib.Path, optional
        The fallback path when the specified path was not found

    Returns
    -------
    pathlib.Path or None
        The path object to the resource on the filesystem
    """

    if fallback_path is not None and isinstance(fallback_path, str):
        fallback_path = Path(fallback_path).expanduser()

    if path is not None and isinstance(path, str):
        path = Path(path).expanduser()

    if path is not None and path.exists():
        return path
    if fallback_path is not None and fallback_path.exists():
        return fallback_path


def generate_extension(extension: str) -> str:
    """Generate a regex pattern to check lower and upper case extensions

    Thanks to https://stackoverflow.com/a/10148272

    Parameters
    ----------
    extension : str
        Extension to parse without the first dot

    Returns
    -------
    str
        Regex pattern

    Examples
    --------
    >>> generate_extension("nes")
    '[nN][eE][sS]'
    """

    pattern = str()

    for character in extension:
        if not character == ".":
            pattern += f"[{character.lower()}{character.upper()}]"

        else:
            pattern += "."

    return pattern


def generate_identifier(path: str | Path, use_inode: bool = True) -> str:
    """Generate an identifier from a path

    Parameters
    ----------
    path : pathlib.Path or str
        Path to parse into indentifier
    use_inode : bool, default: True
        Add the file inode on filesystem at the end of the identifier

    Returns
    -------
    str
        Identifier string

    Examples
    --------
    >>> generate_identifier('Double Dragon (Europe).nes')
    'double-dragon-europe-nes-25953832'
    >>> generate_identifier('Double Dragon (Europe).nes', use_inode=False)
    'double-dragon-europe-nes'
    """

    if isinstance(path, str):
        path = Path(path).expanduser()

    # Retrieve only alphanumeric element from filename
    name = sub(r"[^\w\d]+", " ", path.name.lower())
    # Remove useless spaces and replace the others with a dash
    name = sub(r"[\s|_]+", "-", name.strip())

    # Retrieve file inode number
    if use_inode and path.exists():
        inode = path.stat().st_ino
        if inode > 0:
            name = f"{name}-{inode}"

    return name


def get_binary_path(binary: str) -> list[Path]:
    """Get a list of available binary paths from $PATH variable

    This function get all the path from $PATH variable which match binary
    request.

    Parameters
    ----------
    binary : str
        Binary name or path

    Returns
    -------
    list
        List of available path

    Examples
    --------
    >>> get_binary_path("ls")
    ['/bin/ls']
    """

    available = []
    if binary is None or len(binary) == 0:
        return available

    binary = Path(binary).expanduser()
    if binary.exists():
        available.append(binary.name)

    for directory in set(environ["PATH"].split(":")):
        path = Path(directory).expanduser()

        if access(path, R_OK):
            binary_path = path.joinpath(binary)

            if binary_path.exists() and binary_path.name not in available:
                available.append(str(binary_path))

    return available


def get_boot_datetime_as_timestamp(proc_path: Path | str = "/proc") -> float:
    """Retrieve boot datetime as timestamp

    The 'uptime' file contains two values: boot time and idle time. This method
    only retrieve the first one to calculate the boot datetime.

    Parameters
    ----------
    proc_path : pathlib.Path or str, optional
        Process file system path (Only used to test the method)

    Returns
    -------
    float
        Boot datetime as timestamp value

    Raises
    ------
    FileNotFoundError
        When the /proc directory do not exists on filesystem
        When the /proc/uptime file do not exists on filesystem
    """

    if isinstance(proc_path, str):
        proc_path = Path(proc_path)

    if not proc_path.exists():
        raise FileNotFoundError("Cannot found process file system")

    uptime_file = proc_path.joinpath("uptime")
    if not uptime_file.exists():
        raise FileNotFoundError(f"Cannot found {uptime_file} on filesystem")

    with uptime_file.open("r") as pipe:
        content = pipe.read().split("\n")[0]

    if content:
        return datetime.now().timestamp() - float(content.split()[0])

    return None


def get_creation_datetime(path: Path | str) -> datetime:
    """Retrieve the creation date from a specific filename

    Parameters
    ----------
    path : pathlib.Path or str
        Path to retrieve creation datetime

    Returns
    -------
    datetime.datetime
        Creation datetime object

    Examples
    --------
    >>> get_creation_datetime("~/.bashrc")
    datetime.datetime(2019, 9, 22, 14, 1, 37, 56527)
    """

    if isinstance(path, str):
        path = Path(path).expanduser()

    if not path.exists():
        return None

    return datetime.fromtimestamp(getctime(path))


def get_data(*args, egg: str = "geode_gem") -> Path:
    """Provides easy access to data in a python egg or local folder

    This function search a path in a specific python egg or in local folder.
    The local folder is check before egg to allow quick debugging.

    Thanks Deluge :)

    Parameters
    ----------
    args : list
        File path as strings list
    egg : str, optional
        Python egg name (Default: 'geode_gem')

    Returns
    -------
    pathlib.Path
        File path instance
    """

    path = Path(*args).expanduser()

    try:
        data = resource_from_path(egg, *args)
    except Exception:
        data = Path(egg).joinpath(path)

    if data.exists():
        return data

    return path


def glob_escape(string: str) -> str:
    """Generate a valid escaped string for glob pattern expansion

    The glob.escape method do not replace the ] character with []] and this
    replacement is mandatory to have a valid pattern for glob.

    With glob.escape: [A] → [[]A]
    With this method: [A] → [[]A[]]

    Parameters
    ----------
    string : str
        String to escape for glob processing

    Returns
    -------
    str
        Escaped string

    Examples
    --------
    >>> glob_escape("[E]")
    '[[]E[]]'
    >>> glob_escape("[E][!]")
    '[[]E[]][[]![]]'
    """

    strings = [
        ("[", "[["),
        ("]", "]]"),
        ("[[", "[[]"),
        ("]]", "[]]"),
        ("?", "[?]"),
        ("*", "[*]"),
    ]

    pattern = str(string)
    for replacer in strings:
        pattern = pattern.replace(*replacer)

    return pattern


def replace_special_keys_from_pattern(
    game: "GenericGame", pattern: str, prefix: str = "<", suffix: str = ">"
) -> Path:
    """Replace the special keys for the specified pattern

    Parameters
    ----------
    game : geode_gem.core.game.GenericGame
        The instance of the game which will be used to replace the special keys
    pattern : str
        The pattern which contains special keys to replace
    prefix : str, default: '<'
        The string used to start the special key pattern
    suffix : str, default: '>'
        The string used to end the special key pattern

    Returns
    -------
    pathlib.Path
        The replaced pattern as a Path object
    """

    data = []

    if game.path is not None:
        data.extend(
            [
                ("filename", game.path.stem),
                ("filepath", game.path),
                ("lower-filename", game.path.stem.lower()),
            ]
        )

    if (
        game.collection is not None
        and hasattr(game.collection, "path")
        and game.collection.path is not None
    ):
        data.append(("collection", game.collection.path))

    if hasattr(game, "game_id"):
        data.append(("game-id", game.game_id))

    for key, value in data:
        element = f"{prefix}{key}{suffix}"
        if element in pattern:
            pattern = pattern.replace(element, glob_escape(value))

    return Path(pattern).expanduser().resolve()


def resource_from_path(egg: str, *args) -> Path | None:
    """Retrieve path from Python module resource data

    Parameters
    ----------
    egg : str
        Python egg name
    args : list
        File path as strings list

    Returns
    -------
    pathlib.Path or None
        The path to the resource object, None otherwise
    """

    # Generate a module name based on egg name and file resource path
    module_name = ".".join([egg, *args[:-1]])

    filepath = resources.files(module_name).joinpath(args[-1])
    return filepath.expanduser()


def save_config_path(*args) -> Path:
    """Saving configuration settings

    Returns
    -------
    pathlib.Path
        The path related to the user configuration directory

    See Also
    --------
    xdg.BaseDirectory.save_config_path
    """

    try:
        from xdg.BaseDirectory import xdg_config_home
    except ImportError:
        xdg_config_home = getenv("XDG_CONFIG_HOME", default="~/.config")

    path = Path(xdg_config_home).expanduser()
    path.mkdir(parents=True, exist_ok=True)

    return path.joinpath(*args)


def save_data_path(*args) -> Path:
    """Saving application data settings

    Returns
    -------
    pathlib.Path
        The path related to the user application data directory

    See Also
    --------
    xdg.BaseDirectory.save_data_path
    """

    try:
        from xdg.BaseDirectory import xdg_data_home
    except ImportError:
        xdg_data_home = getenv("XDG_DATA_HOME", default="~/.local/share")

    path = Path(xdg_data_home).expanduser()
    path.mkdir(mode=0o755, parents=True, exist_ok=True)

    return path.joinpath(*args)
