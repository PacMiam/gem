# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from os import getpgid, getpid
from typing import NoReturn

from geode_gem.core.utils import (
    are_equivalent_timestamps,
    get_boot_datetime_as_timestamp,
)


def read_lock_file(path: Path, timestamp: str) -> bool:
    """Read the specified lock file and compare the content with the timestamp

    Parameters
    ----------
    path : pathlib.Path
        The path of the lock file
    timestamp : str
        The value of the timestamp to compare with the lock file content

    Returns
    -------
    bool
        True if the application is currently locked, False otherwise
    """

    content = path.read_text()
    if len(content) == 0:
        return False

    try:
        pid, previous_timestamp = content.split()

        getpgid(int(pid))

        if are_equivalent_timestamps(timestamp, previous_timestamp, delta=4):
            return True

        return False

    except ProcessLookupError:
        return False

    except ValueError:
        return False


def write_lock_file(path: Path, pid: int, timestamp: float):
    """Write the PID and the current timestamp to the specified lock file

    Parameters
    ----------
    path : pathlib.Path
        The path of the lock file
    pid : int
        The process identifier of the current application
    timestamp : float
        The current timestamp
    """

    with path.open("wb") as fb:
        # Register current PID into file
        fb.write(bytes(f"{pid} {timestamp}", "UTF-8"))

        try:
            from fcntl import flock, LOCK_EX, LOCK_NB

            # Perform the lock operation on file
            flock(fb, LOCK_EX | LOCK_NB)

        except ImportError:
            pass


def init_lock_file(path: Path) -> NoReturn:
    """Initialize the specified lock file

    Parameters
    ----------
    path : pathlib.Path
        The path of the lock file

    Raises
    ------
    FileNotFoundError
        When the /proc/uptime file was not found on the filesystem. This error
        will occurs in non-UNIX system…
    FileExistsError
        When the specified lock file already exists with a working application
    """

    current_timestamp = get_boot_datetime_as_timestamp()
    if current_timestamp is None:
        raise FileNotFoundError("Cannot access to /proc/uptime file")

    is_lock = False
    if path.exists():
        is_lock = read_lock_file(path, current_timestamp)

    if is_lock:
        raise FileExistsError("Already running with another PID")

    write_lock_file(path, getpid(), current_timestamp)
