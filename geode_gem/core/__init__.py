# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from configparser import NoOptionError
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from logging import getLogger, Logger
from pathlib import Path
from typing import Any, Self, NoReturn

from geode_gem.configuration import Configuration
from geode_gem.core.collection import (
    ConsoleCollection,
    GenericCollection,
    NativeCollection,
    ScummvmCollection,
    WineCollection,
)
from geode_gem.core.database import Database
from geode_gem.core.game import GenericGame
from geode_gem.core.launcher import (
    EmulatorLauncher,
    GenericLauncher,
    NativeLauncher,
    ScummvmLauncher,
    WineLauncher,
)
from geode_gem.core.utils import save_config_path, save_data_path


class Collections(Enum):
    """Available collections for the Geode-Gem application"""

    console = ConsoleCollection
    native = NativeCollection
    scummvm = ScummvmCollection
    wine = WineCollection


class Launchers(Enum):
    """Available launchers for the Geode-Gem application"""

    emulator = EmulatorLauncher
    native = NativeLauncher
    scummvm = ScummvmLauncher
    wine = WineLauncher


@dataclass
class Core:
    """The Core of the Geode-Gem application which manage collections and games

    Attributes
    ----------
    config_path : pathlib.Path
        The path of the geode-gem directory in the user configuration directory
    database : geode_gem.core.database.Database
        The instance of the database for the selected profile
    data_path : pathlib.Path
        The path of the geode-gem directory in the user share data directory
    logger : logging.Logger
        The instance of the logger for this application
    profile_config_path : pathlib.Path
        The path to the config directory with the current profile
    profile_data_path : pathlib.Path
        The path to the data directory with the current profile
    profile_name : str
        The name of the selected profile
    store_collections : dict
        The list of collection instances parsed from the selected profile
    store_launchers : dict
        The list of launcher instances parsed from the selected profile

    Examples
    --------
    >>> core = Core()
    >>> core.read_profile()
    """

    store_collections: dict[str, Any] = field(default_factory=dict)
    store_launchers: dict[str, Any] = field(default_factory=dict)

    config_path: Path = field(default=save_config_path("geode-gem"))
    data_path: Path = field(default=save_data_path("geode-gem"))
    logger: Logger = field(default=getLogger(__name__))

    database: Database | None = None
    profile_config_path: Path | None = None
    profile_data_path: Path | None = None
    profile_name: str | None = None

    def __instantiate__(
        self: Self,
        path: Path,
        enum: Collections | Launchers,
        fallback: GenericCollection | GenericLauncher,
    ) -> GenericCollection | GenericLauncher | None:
        """Generate the object based on the specified configuration

        Parameters
        ----------
        path : pathlib.Path
            The path to the configuration file used to retrieve the object
            information
        enum : geode.core.Collections | geode_gem.core.Launchers
            The enumation class which contains the object classes
        fallback : geode_gem.core.collection.GenericCollection or
                   geode_gem.core.launcher.GenericLauncher
            The class to use when the specified metadata type do not exists in
            the specified enum class

        Returns
        -------
        geode_gem.core.collection.GenericCollection or
        geode_gem.core.launcher.GenericLauncher or None
            The instantiated object or None if an error occurs
        """

        try:
            configuration = Configuration(path)
            enum_type = configuration.get("metadata", "type")

            try:
                instance = enum[enum_type].value
            except KeyError:
                instance = fallback

            return instance.new_from_configuration(configuration)

        except NoOptionError as error:
            self.logger.error(
                f"The option '{error.option}' is missing from the section "
                f"'{error.section}' in the file '{configuration.path}'"
            )
        except Exception as error:
            self.logger.exception(error)
            self.logger.warning(
                f"Cannot parse configuration '{configuration.path}': {error}"
            )

        return None

    @property
    def collections(self: Self) -> list[GenericCollection]:
        """Retrieve the list of collections

        Returns
        -------
        list[geode_gem.core.collection.GenericCollection]
            The list of all the collections stored in the current profile
        """

        return list(self.store_collections.values())

    @property
    def launchers(self: Self) -> list[GenericLauncher]:
        """Retrieve the list of launchers

        Returns
        -------
        list[geode_gem.core.launcher.GenericLauncher]
            The list of all the launchers stored in the current profile
        """

        return list(self.store_launchers.values())

    def commit_database(self: Self) -> NoReturn:
        """Commit transactions which have not been commited yet from database"""

        self.database.commit()

    def get_collection(self: Self, identifier: str) -> GenericCollection | None:
        """Retrieve a specific collection based on his identifier

        Parameters
        ----------
        identifier : str
            The identifier of the collection to retrieve from the cache

        Returns
        -------
        geode_gem.core.collection.GenericCollection or None
            The instance of the collection if exists, None otherwise
        """

        return self.store_collections.get(identifier, None)

    def get_launcher(self: Self, identifier: str) -> GenericLauncher | None:
        """Retrieve a specific launcher based on his identifier

        Parameters
        ----------
        identifier : str
            The identifier of the launcher to retrieve from the cache

        Returns
        -------
        geode_gem.core.launcher.GenericLauncher or None
            The instance of the launcher if exists, None otherwise
        """

        return self.store_launchers.get(identifier, None)

    def get_path_from_config(self: Self, *args) -> Path:
        """Retrieve a path in configuration directory from current profile

        Returns
        -------
        pathlib.Path
            The full path pointed to specified parameters
        """

        return self.profile_config_path.joinpath(*args)

    def get_path_from_data(self: Self, *args) -> Path:
        """Retrieve a path in local data directory from current profile

        Returns
        -------
        pathlib.Path
            The full path pointed to specified parameters
        """

        return self.profile_data_path.joinpath(*args)

    def read_profile(self: Self, profile: str = "default") -> NoReturn:
        """Read the configuration files from the specified profile

        Parameters
        ----------
        profile : str
            The name of the profile used to retrieve the configuration files
        """

        timer = datetime.now()

        if not self.profile_name == profile:
            self.logger.debug(f"Prepare the profile {profile} for reading")
            self.profile_config_path = self.config_path.joinpath(profile)
            self.profile_config_path.mkdir(parents=True, exist_ok=True)

            self.profile_data_path = self.data_path.joinpath(profile)
            self.profile_data_path.mkdir(parents=True, exist_ok=True)

            self.profile_name = profile

        # Sanitize the main database by checking the schemata
        self.database = Database(self.get_path_from_data("geode-gem.db"))
        self.database.check_schemata()

        # Retrieve the launchers from the profile directory
        launchers_path = self.get_path_from_config("launchers")
        launchers_path.mkdir(exist_ok=True)

        self.store_launchers.clear()
        for path in launchers_path.glob("*.conf"):
            data = self.__instantiate__(path, Launchers, GenericLauncher)
            if data is not None:
                self.logger.debug(f"Register the launcher {data.identifier}")
                self.store_launchers[data.identifier] = data

        # Retrieve the collections from the profile directory
        collections_path = self.get_path_from_config("collections")
        collections_path.mkdir(exist_ok=True)

        self.store_collections.clear()
        for path in collections_path.glob("*.conf"):
            data = self.__instantiate__(path, Collections, GenericCollection)
            if data is not None:
                # Populate the games storage for this collection
                data.inspect(self)
                # Register the launcher associated with this collection
                if data.launcher is not None:
                    data.launcher = self.get_launcher(data.launcher)

                self.logger.debug(f"Register the collection {data.identifier}")
                self.store_collections[data.identifier] = data

        timer = datetime.now() - timer
        self.logger.debug(f"The profile was readed in {timer.total_seconds()}s")

    def save_game(
        self: Self, game: GenericGame, commit: bool = True
    ) -> NoReturn:
        """Save the information from the specified game into the database

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game to save into the database
        commit : bool, default: True
            Commit the transaction in the database at the end of the process
        """

        if not game.checksum:
            game.calc_checksum()

        self.database.save_game(game, commit=commit)

    def set_path_for_game(
        self: Self, game: GenericGame, category: str, extension: str
    ) -> Path:
        """Generate the path to a game resource from the profile data directory

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to generate the log file path
        category : str
            The name of the directory where the file will be stored
        extension : str
            The extension of the file to generate

        Returns
        -------
        pathlib.Path
            The generated path with the specified extension
        """

        path = self.get_path_from_data(category, game.collection.identifier)

        return path.joinpath(f"{game.identifier}.{extension}")

    def update_game_from_database(self: Self, game: GenericGame) -> GenericGame:
        """Update the specified game with information stored in the database

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game to update from database content

        Returns
        -------
        geode_gem.core.game.GenericGame
            The new instance of the updated game
        """

        if len(game.checksum) == 0:
            game.calc_checksum()

        data = self.database.get_data_from_game(game)
        if len(data) > 0:
            game.update(data)

        if isinstance(game.launcher, str):
            game.launcher = self.get_launcher(game.launcher)

        game.log_path = self.set_path_for_game(game, "logs", "log")
        game.note_path = self.set_path_for_game(game, "notes", "md")

        return game
