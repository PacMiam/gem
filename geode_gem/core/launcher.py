# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field
from pathlib import Path
from shlex import join, split
from typing import NoReturn, Self

from geode_gem.configuration import Configuration
from geode_gem.core.entity import Entity
from geode_gem.core.game import (
    ConsoleGame,
    GenericGame,
    NativeGame,
    ScummvmGame,
    WineGame,
)
from geode_gem.core.utils import check_path, replace_special_keys_from_pattern


@dataclass
class ProcessData:
    """Represents the command to call with the subprocess module

    Attributes
    ----------
    command_line : list[str]
        The command line to call with the subprocess.run function as list of
        string arguments
    environment : dict[str, str]
        The environment variables associated with the command
    path : pathlib.Path or None
        The path where the command must be started (by default, the current
        path is used)
    """

    command_line: list[str] = field(default_factory=list)
    environment: dict[str, str] = field(default_factory=dict)
    path: Path | None = None


@dataclass
class GenericLauncher(Entity):
    """Represents a generic launcher"""

    def get_environment_for_game(
        self: Self, game: GenericGame
    ) -> dict[str, str]:
        """Generate the environment variables storage based on the game object

        Notes
        -----
        It will take the launcher environment first, then the collection related
        to the game, then the game itself.

        Returns
        -------
        dict[str, str]
            The environment variables as a dictionary structure
        """

        environment = self.environment.copy()

        if game.collection is not None:
            environment.update(game.collection.environment)

        environment.update(game.environment)

        return environment

    def get_screenshots(self: Self, game: GenericGame) -> list[Path]:
        """Retrieve the screenshots associate with the game

        Notes
        -----
        The generic get_screenshots method do not returns anything, since there
        is no way to define a global screenshots directory using the correct
        format for all the GNU/Linux distributions…

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to fetch the list of screenshots

        Returns
        -------
        list[pathlib.Path]
            The list of screenshots as Path objects
        """

        return []

    def on_pre_shutdown(
        self: Self, game: GenericGame, processus_id: int
    ) -> ProcessData | None:
        """Run a command before the game termination in the thread process

        Notes
        -----
        This method must be overwritten to return a list which contains the
        command to run

        Parameters
        ----------
        processus_id : int
            The identifier of the running processus

        Returns
        -------
        geode_gem.core.launcher.ProcessData
            The object which contains all the information to run the pre
            shutdown hook
        """

        return None

    def on_post_shutdown(
        self: Self, game: GenericGame, processus_id: int
    ) -> ProcessData | None:
        """Run a command after the game was terminated in the thread process

        Notes
        -----
        This method must be overwritten to return a list which contains the
        command to run

        Parameters
        ----------
        processus_id : int
            The identifier of the running processus

        Returns
        -------
        geode_gem.core.launcher.ProcessData
            The object which contains all the information to run the post
            shutdown hook
        """

        return None

    def on_startup(self: Self, game: GenericGame) -> ProcessData | None:
        """Run a command before the game start in the thread process

        Notes
        -----
        This method must be overwritten to return a list which contains the
        command to run

        Returns
        -------
        geode_gem.core.launcher.ProcessData
            The object which contains all the information to run the startup
            hook
        """

        return None


@dataclass
class EmulatorLauncher(GenericLauncher):
    """Represents an emulator launcher to use with a ROM game

    Attributes
    ----------
    configuration : pathlib.Path or None
        The path to the emulator configuration file
    default : str or None
        The default arguments used to execute the launcher binary
    executable : str or None
        The name or the file path of the launcher binary
    fullscreen : str or None
        The argument used to define the fullscreen mode for this emulator
    savestates : str or None
        The pattern used to retrieve the savestates related to a game
    screenshots : str or None
        The pattern used to retrieve the screenshots related to a game
    windowed : str or None
        The argument used to define the windowed mode for this emulator
    """

    configuration: Path = None

    default: str = ""
    executable: str = None
    fullscreen: str = ""
    savestates: str = ""
    screenshots: str = ""
    windowed: str = ""

    @staticmethod
    def parse_configuration(parser: Configuration):
        data = GenericLauncher.parse_configuration(parser)
        data["configuration"] = parser.getpath(
            "emulator", "configuration", fallback=None
        )
        data["executable"] = parser.get("metadata", "executable", fallback=None)
        data["default"] = parser.get("arguments", "default", fallback="")
        data["fullscreen"] = parser.get("arguments", "fullscreen", fallback="")
        data["savestates"] = parser.get("patterns", "savestates", fallback="")
        data["screenshots"] = parser.get("patterns", "screenshots", fallback="")
        data["windowed"] = parser.get("arguments", "windowed", fallback="")

        return data

    def get_screenshots(self: Self, game: ConsoleGame) -> list[Path]:
        """Retrieve the screenshots associate with the game"""

        pattern = replace_special_keys_from_pattern(game, self.screenshots)
        return [
            path for path in pattern.parent.glob(pattern.name) if path.is_file()
        ]

    def run(
        self: Self, game: ConsoleGame, fullscreen: bool = False
    ) -> ProcessData | None:
        """Generate the command line to run the ROM with a specific emulator

        Parameters
        ----------
        game : geode_gem.core.game.ConsoleGame
            The instance of the ROM Game to call with the emulator launcher
        fullscreen : bool
            Activate the fullscreen mode when started the emulator

        Returns
        -------
        geode_gem.core.launcher.ProcessData or None
            The object which contains all the information to run the game
        """

        if (
            self.executable is not None
            and game.path is not None
            and game.path.exists()
        ):
            command_line = [self.executable]

            if fullscreen and len(self.fullscreen) > 0:
                command_line.append(self.fullscreen)
            elif not fullscreen and len(self.windowed) > 0:
                command_line.append(self.windowed)

            if len(self.default) > 0:
                command_line.append(self.default)

            command_line.append(str(game.path))

            return ProcessData(
                command_line=split(join(command_line)),
                environment=self.get_environment_for_game(game),
            )


@dataclass
class NativeLauncher(GenericLauncher):
    """Represents a launcher used to call native command on the system

    Notes
    -----
    This launcher will only serve to generate the command line from the
    DesktopEntry file. Having a dedicated class will provide the possibility to
    add environment variables specific to a launcher (eg. forcing the usage of
    a specific DISPLAY).
    """

    def run(self: Self, game: NativeGame, **kwargs) -> ProcessData | None:
        """Generate the command line to run the Desktop entry

        Parameters
        ----------
        game : geode_gem.core.game.NativeGame
            The instance of the native Game used to retrieve the executable

        Returns
        -------
        geode_gem.core.launcher.ProcessData or None
            The object which contains all the information to run the game
        """

        if game.executable is not None:
            return ProcessData(
                command_line=split(game.executable),
                environment=self.get_environment_for_game(game),
            )


@dataclass
class ScummvmLauncher(GenericLauncher):
    """Represents the ScummVM launcher

    More information on https://www.scummvm.org/
    """

    def run(self: Self, game: ScummvmGame, **kwargs) -> ProcessData | None:
        """Generate the command line to run the game with Scummvm

        Parameters
        ----------
        game : geode_gem.core.game.ScummvmGame
            The instance of the Scummvm game used to retrieve information

        Returns
        -------
        geode_gem.core.launcher.ProcessData or None
            The object which contains all the information to run the game
        """

        if game.collection is not None and game.collection.path is not None:
            return ProcessData(
                command_line=split(
                    f"scummvm -c {game.collection.path} {game.identifier}"
                ),
                environment=self.get_environment_for_game(game),
            )


@dataclass
class WineLauncher(GenericLauncher):
    """Represents a launcher dedicated to run .exe binary with WINE

    Attributes
    ----------
    wine_binaries : pathlib.Path
        The path of the wine binaries directory (by default /usr/bin)
    """

    wine_binaries: Path = None

    def __post_init__(self: Self) -> NoReturn:
        """Update the binaries directory if this one is missing"""

        if self.wine_binaries is None:
            self.wine_binaries = Path("/usr/bin")

    def get_binary_path(self: Self, binary: str) -> Path:
        """Get the path to a specific binary from the binaries directory

        Parameters
        ----------
        binary : str
            The name of the binary (eg. wine)

        Returns
        -------
        pathlib.Path
            The path to the binary
        """

        return self.wine_binaries.joinpath(binary)

    def get_environment_for_game(self: Self, game: WineGame) -> dict[str, str]:
        """Generate the environment variables storage based on the game object

        Notes
        -----
        The following environment variables needs to be defined to start WINE
        with a specific environment:
        - WINELOADER: path to the directory which contains the WINE binaries
                      (eg. /usr/bin)
        - WINEPREFIX: path to the directory where the prefix has been
                      initialized (eg. ~/.wine)
        - WINESERVER: path to the wineserver binary, which allow to set a
                      different server (eg. /usr/bin/wineserver)
        - WINEDEBUG: activate the debug mode for WINE with the `-all` parameter
        """

        environment = super().get_environment_for_game(game)
        environment["WINELOADER"] = str(self.wine_binaries)
        environment["WINEPREFIX"] = str(game.wine_prefix)
        environment["WINESERVER"] = str(self.get_binary_path("wineserver"))

        if game.wine_debug:
            environment["WINEDEBUG"] = "-all"

        return environment

    def on_post_shutdown(
        self: Self, game: WineGame, processus_id: int
    ) -> ProcessData:
        """Force the shutdown of the wine process if it still alive"""

        path = check_path(f"/proc/{processus_id}")

        if path is not None and path.exists():
            return ProcessData(
                command_line=split(f"{self.get_binary_path('wineserver')} -k"),
                environment=self.get_environment_for_game(game),
            )

    def on_startup(self: Self, game: WineGame) -> ProcessData:
        """Use the startup hook to initialize the wine prefix if needed"""

        if not game.wine_prefix.exists():
            return ProcessData(
                command_line=split(f"{self.get_binary_path('wineboot')} -u"),
                environment=self.get_environment_for_game(game),
            )

    @staticmethod
    def parse_configuration(parser: Configuration) -> NoReturn:
        data = GenericLauncher.parse_configuration(parser)
        data["wine_binaries"] = parser.getpath(
            "wine", "binaries", fallback=None
        )

        return data

    def run(self: Self, game: WineGame, **kwargs) -> ProcessData | None:
        """Generate the command line to run the game with WINE

        Parameters
        ----------
        game : geode_gem.core.game.WineGame
            The instance of the WINE game used to retrieve the executable

        Returns
        -------
        geode_gem.core.launcher.ProcessData or None
            The object which contains all the information to run the game
        """

        if game.executable is not None:
            return ProcessData(
                command_line=split(
                    f"{self.get_binary_path('wine')} {game.executable}"
                ),
                environment=self.get_environment_for_game(game),
            )
