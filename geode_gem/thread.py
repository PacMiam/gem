# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from collections.abc import Callable
from datetime import datetime
from io import TextIOWrapper
from logging import Logger
from os import environ
from pathlib import Path
from shlex import join
from subprocess import CompletedProcess, PIPE, Popen, TimeoutExpired
from threading import Thread
from typing import NoReturn, Self, TYPE_CHECKING

if TYPE_CHECKING:
    from geode_gem.core.game import GenericGame
    from geode_gem.core.launcher import GenericLauncher


class GameThread(Thread):
    """Represents the thread which will execute the game processus

    Attributes
    ----------
    launcher : geode_gem.core.launcher.GenericLauncher
        The instance of the launcher used to generate the command line to start
        the game
    game : geode_gem.core.game.GenericGame
        The instance of the game which will be started
    logger : logging.Logger
        The instance of the logger used to print message in stdout
    emit : callback
        The callback from the main core used to call method in the main thread
    processus : subprocess.CompletedProcess
        The information of the executed process when this one was terminated,
        None otherwise
    started_date : datetime.datetime
        The date when the process have been started
    terminated_date : datetime.datetime
        The date when the process have been terminated
    """

    def __init__(
        self: Self,
        launcher: "GenericLauncher",
        game: "GenericGame",
        logger: Logger,
        emit: Callable[[str, "GenericGame", datetime, datetime], NoReturn],
    ) -> NoReturn:
        """Prepare the thread

        Parameters
        ----------
        launcher : geode_gem.core.launcher.GenericLauncher
            The instance of the launcher
            the game
        game : geode_gem.core.game.GenericGame
            The instance of the game
        logger : logging.Logger
            The instance of the logger
        emit : callback
            The callback from the main core
        """

        super().__init__(daemon=True, name=game.identifier)

        self.launcher = launcher
        self.game = game
        self.logger = logger
        self.emit = emit

        self.processus: CompletedProcess = None
        self.started_date: datetime = None
        self.terminated_date: datetime = None

    def prepare_command(
        self: Self,
        file_buffer: TextIOWrapper,
        command_line: list[str],
        environment_variables: dict[str, str],
        text: str,
    ) -> Popen:
        """Prepare the subprocess for the specified command line

        Parameters
        ----------
        file_buffer : io.TextIOWrapper
            The currently open buffer where the subprocess output will be
            redirected
        command_line : list[str]
            The command to run as a list of strings
        environment_variables : dict[str, str]
            Additionnal environment variables to add in the subprocess
        text : str
            The text to write in the buffer before the subprocess preparation

        Returns
        -------
        subprocess.Popen
            The subprocess instance for the specified command line
        """

        # Avoid to raise an UnicodeEncodeError when filename contains
        # specials unicode characters
        printable_command = join(command_line).encode(errors="ignore").decode()

        self.print_line_in_buffer(
            file_buffer, f">>> {text}: {printable_command}"
        )

        return Popen(
            command_line,
            cwd=Path("~").expanduser(),
            env=environment_variables,
            stdin=PIPE,
            stderr=file_buffer,
            stdout=file_buffer,
            start_new_session=True,
            universal_newlines=True,
        )

    def print_line_in_buffer(
        self: Self, file_buffer: TextIOWrapper, text: str
    ) -> NoReturn:
        """Print a line in the text buffer

        Parameters
        ----------
        file_buffer : io.TextIOWrapper
            The currently open buffer where the text will be redirected
        text : str
            The string to add in the buffer
        """

        file_buffer.write(f"{text}\n")
        file_buffer.flush()

    def run(self: Self) -> NoReturn:
        """Start the game and log the process in a log file

        See Also
        --------
        subprocess.Popen
        geode_gem.application.Application.emit
        """

        return_code = None

        self.started_date = datetime.now()
        self.logger.debug(f"The game have been started at {self.started_date}")

        process_data = self.launcher.run(self.game)

        environment_variables = environ.copy()
        environment_variables.update(process_data.environment)

        if not self.game.log_path.parent.exists():
            self.game.log_path.parent.mkdir(parents=True, exist_ok=True)

        with self.game.log_path.open("w") as file_buffer:
            try:
                self.print_line_in_buffer(
                    file_buffer,
                    f">>> Started date: {self.started_date.strftime('%x %X')}",
                )

                # Run the on_startup hook if available
                if (data := self.launcher.on_startup(self.game)) is not None:
                    self.prepare_command(
                        file_buffer,
                        data.command_line,
                        environment_variables.update(data.environment),
                        "Run the startup hook",
                    ).communicate()

                self.processus = self.prepare_command(
                    file_buffer,
                    process_data.command_line,
                    environment_variables.copy(),
                    "Run the command",
                )
                self.processus.communicate()

                return_code = self.processus.returncode

            except OSError as error:
                self.logger.error(f"Cannot access to game: {error}")
                self.emit("crash-game", self.game, error)

            except MemoryError as error:
                self.logger.error(f"A memory error occur: {error}")
                self.emit("crash-game", self.game, error)

            except KeyboardInterrupt:
                self.logger.info("Terminate by keyboard interrupt")

            except Exception as error:
                self.logger.exception(error)
                self.emit("crash-game", self.game, error)

            # Run the on_pre_shutdown hook if available
            if (
                self.processus is not None
                and (
                    data := self.launcher.on_pre_shutdown(
                        self.game, self.processus.pid
                    )
                )
                is not None
            ):
                self.prepare_command(
                    file_buffer,
                    data.command_line,
                    environment_variables.copy().update(data.environment),
                    "Run the pre shutdown hook",
                ).communicate()

            # The program was closed
            if return_code is not None:
                # This processus do not ends very well...
                if return_code > 0:
                    self.logger.error(
                        "The processus was terminated with the code "
                        f"{return_code}. Check the game's log file for more "
                        "information."
                    )
                    self.emit("crash-game", self.game, OSError(return_code))

            # The program still running and its anormal
            else:
                # Send the SIGTERM signal to the processus
                if self.processus is not None:
                    self.logger.debug(
                        "The SIGTERM signal have been sent to the processus"
                    )
                    self.processus.terminate()

                # Send the SIGKILL signal to the processus if it still alive…
                if self.processus is not None:
                    try:
                        self.logger.debug(
                            "The processus still exists, wait 5 seconds before "
                            "sending the SIGKILL signal"
                        )
                        self.processus.wait(5)

                    except TimeoutExpired:
                        self.logger.debug(
                            "The SIGKILL signal have been sent to the processus"
                        )
                        self.processus.kill()

                        self.print_line_in_buffer(
                            file_buffer,
                            ">>> The SIGKILL signal have been sent to the "
                            "processus",
                        )

            self.terminated_date = datetime.now()

            # Run the on_post_shutdown hook if available
            if (
                self.processus is not None
                and (
                    data := self.launcher.on_post_shutdown(
                        self.game, self.processus.pid
                    )
                )
                is not None
            ):
                self.prepare_command(
                    file_buffer,
                    data.command_line,
                    environment_variables.copy().update(data.environment),
                    "Run the post shutdown hook",
                ).communicate()

            self.logger.debug(
                f"The game have been terminated at {self.terminated_date}"
            )
            self.print_line_in_buffer(
                file_buffer,
                f">>> Terminated: {self.terminated_date.strftime('%x %X')}",
            )

        self.emit(
            "terminate-game", self.game, self.started_date, self.terminated_date
        )
