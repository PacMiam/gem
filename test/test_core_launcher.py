# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from os import getpid
from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest import TestCase

from geode_gem.core.configuration import Configuration
from geode_gem.core.collection import GenericCollection, ScummvmCollection
from geode_gem.core.game import (
    GenericGame,
    ConsoleGame,
    NativeGame,
    ScummvmGame,
    WineGame,
)
from geode_gem.core.launcher import (
    GenericLauncher,
    EmulatorLauncher,
    NativeLauncher,
    ScummvmLauncher,
    WineLauncher,
)


EMULATOR_VALID_CONFIG = """
[metadata]
identifier = test-emu
label = My Awesome Emulator
executable = ls

[arguments]
default = -lh

[patterns]
screenshots = <name>.png
"""


class GenericLauncherTestCase(TestCase):
    def test_get_environment_for_game_with_collection(self):
        game = GenericGame("test")
        game.collection = GenericCollection("test")
        launcher = GenericLauncher("test")

        game.collection.putenv("DISPLAY", ":0")
        self.assertCountEqual(
            launcher.get_environment_for_game(game), {"DISPLAY": ":0"}
        )

        game.putenv("DISPLAY", ":1")
        self.assertCountEqual(
            launcher.get_environment_for_game(game), {"DISPLAY": ":1"}
        )

    def test_get_environment_for_game_without_collection(self):
        game = GenericGame("test")
        launcher = GenericLauncher("test")
        self.assertEqual(launcher.get_environment_for_game(game), {})

        launcher.putenv("DISPLAY", ":0")
        self.assertCountEqual(
            launcher.get_environment_for_game(game), {"DISPLAY": ":0"}
        )

        game.putenv("DISPLAY", ":1")
        self.assertCountEqual(
            launcher.get_environment_for_game(game), {"DISPLAY": ":1"}
        )

    def test_get_screenshots(self):
        game = GenericGame("test")
        launcher = GenericLauncher("test")
        self.assertEqual(len(launcher.get_screenshots(game)), 0)

    def test_on_pre_shutdown(self):
        game = GenericGame("test")
        self.assertIsNone(GenericLauncher("test").on_pre_shutdown(game, 42))

    def test_on_post_shutdown(self):
        game = GenericGame("test")
        self.assertIsNone(GenericLauncher("test").on_post_shutdown(game, 42))

    def test_on_startup(self):
        game = GenericGame("test")
        self.assertIsNone(GenericLauncher("test").on_startup(game))


class EmulatorLauncherTestCase(TestCase):
    MINIMAL_CONFIG = """
    [metadata]
    identifier = test

    [emulator]
    configuration = ~/.emulator/config.ini

    [patterns]
    screenshots = /tmp/*.png
    """

    def test_constructor_from_configuration(self):
        config = Configuration()
        config.read_string(self.MINIMAL_CONFIG)

        launcher = EmulatorLauncher.new_from_configuration(config)
        self.assertEqual(launcher.identifier, "test")
        self.assertEqual(str(launcher.screenshots), "/tmp/*.png")

    def test_get_screenshots(self):
        with TemporaryDirectory() as context:
            path = Path(context)
            for index in range(4):
                path.joinpath(f"game-{index}.png").touch()

            launcher = EmulatorLauncher(
                "test", screenshots=str(path.joinpath("<lower-filename>-*.png"))
            )

            game = ConsoleGame("test", path=Path("Game.nes"))
            screenshots = launcher.get_screenshots(game)

        self.assertEqual(len(screenshots), 4)
        self.assertEqual(screenshots[2].stem, "game-2")

    def test_run(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            path.touch()
            game = ConsoleGame("test", path=path)

            launcher = EmulatorLauncher(
                "test",
                executable="mednafen",
                fullscreen="-f",
                windowed="-w",
            )
            process_data = launcher.run(game, fullscreen=False)
            self.assertEqual(
                process_data.command_line, ["mednafen", "-w", str(path)]
            )

            process_data = launcher.run(game, fullscreen=True)
            self.assertEqual(
                process_data.command_line, ["mednafen", "-f", str(path)]
            )

            launcher.default = "--awesome"
            process_data = launcher.run(game, fullscreen=True)
            self.assertEqual(
                process_data.command_line,
                ["mednafen", "-f", "--awesome", str(path)],
            )


class NativeLauncherTestCase(TestCase):
    def test_run(self):
        game = NativeGame("test", executable="pignouf")

        process_data = NativeLauncher("test").run(game)
        self.assertEqual(process_data.command_line, ["pignouf"])


class ScummvmLauncherTestCase(TestCase):
    def test_run(self):
        game = ScummvmGame("test")
        game.collection = ScummvmCollection("test", path=Path("scummvm.ini"))

        process_data = ScummvmLauncher("test").run(game)
        self.assertEqual(
            process_data.command_line, ["scummvm", "-c", "scummvm.ini", "test"]
        )


class WineLauncherTestCase(TestCase):
    MINIMAL_CONFIG = """
    [metadata]
    identifier = test

    [wine]
    binaries = /tmp/wine
    """

    def test_constructor(self):
        launcher = WineLauncher("test")
        self.assertEqual(str(launcher.wine_binaries), "/usr/bin")

    def test_constructor_with_binaries_path(self):
        launcher = WineLauncher("test", wine_binaries=Path("/tmp"))
        self.assertEqual(str(launcher.wine_binaries), "/tmp")

    def test_constructor_from_configuration(self):
        config = Configuration()
        config.read_string(self.MINIMAL_CONFIG)

        launcher = WineLauncher.new_from_configuration(config)
        self.assertEqual(launcher.identifier, "test")
        self.assertEqual(str(launcher.wine_binaries), "/tmp/wine")

    def test_get_binary_path(self):
        launcher = WineLauncher("test")
        self.assertEqual(str(launcher.get_binary_path("wine")), "/usr/bin/wine")

    def test_get_environment_for_game(self):
        launcher = WineLauncher("test")
        self.assertEqual(
            launcher.get_environment_for_game(WineGame("test")),
            {
                "WINELOADER": "/usr/bin",
                "WINEPREFIX": "~/.wine",
                "WINESERVER": "/usr/bin/wineserver",
            },
        )

        launcher = WineLauncher("test", wine_binaries="/opt")
        game = WineGame("test", wine_prefix="/tmp", wine_debug=True)
        self.assertEqual(
            launcher.get_environment_for_game(game),
            {
                "WINEDEBUG": "-all",
                "WINELOADER": "/opt",
                "WINEPREFIX": "/tmp",
                "WINESERVER": "/opt/wineserver",
            },
        )

    def test_on_post_shutdown(self):
        launcher = WineLauncher("test")

        process_data = launcher.on_post_shutdown(WineGame("test"), getpid())
        self.assertEqual(
            process_data.command_line, ["/usr/bin/wineserver", "-k"]
        )

    def test_on_startup(self):
        launcher = WineLauncher("test")

        process_data = launcher.on_startup(
            WineGame("test", wine_prefix=Path("unknown_directory"))
        )
        self.assertEqual(process_data.command_line, ["/usr/bin/wineboot", "-u"])

    def test_run(self):
        launcher = WineLauncher("test")
        self.assertIsNone(launcher.run(WineGame("test")))

        process_data = launcher.run(WineGame("test", executable="pignouf.exe"))
        self.assertEqual(
            process_data.command_line, ["/usr/bin/wine", "pignouf.exe"]
        )
