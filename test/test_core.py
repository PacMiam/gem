# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp, NamedTemporaryFile
from unittest import TestCase

from geode_gem.core import Collections, Core, Launchers
from geode_gem.core.collection import GenericCollection
from geode_gem.core.game import GenericGame
from geode_gem.core.launcher import GenericLauncher


class CoreTestCase(TestCase):
    UNKNOWN_LAUNCHER = """
    [metadata]
    identifier = test
    type = top
    """

    VALID_COLLECTION = """
    [metadata]
    identifier = test
    type = native
    """

    VALID_LAUNCHER = """
    [metadata]
    identifier = test
    type = wine

    [wine]
    binaries = /usr/bin
    """

    def setUp(self):
        self.directory = Path(mkdtemp(suffix="geode-gem"))

        self.core = Core(
            config_path=self.directory.joinpath("config"),
            data_path=self.directory.joinpath("data"),
        )

    def tearDown(self):
        if self.directory.exists():
            rmtree(self.directory)

    def quick_profile(self, profile_name: str = "default"):
        default_path = self.directory.joinpath("config", profile_name)
        default_path.joinpath("collections").mkdir(parents=True)
        default_path.joinpath("launchers").mkdir(parents=True)

        default_path.joinpath("collections", "test.conf").write_text(
            self.VALID_COLLECTION
        )
        default_path.joinpath("launchers", "test.conf").write_text(
            self.VALID_LAUNCHER
        )

        self.core.read_profile(profile_name)

    def test_instantiate_with_known_type(self):
        with NamedTemporaryFile() as context:
            path = Path(context.name)
            path.write_text(self.VALID_COLLECTION)

            collection = self.core.__instantiate__(
                path, Collections, GenericCollection
            )
            self.assertIsInstance(collection, Collections.native.value)
            self.assertEqual(collection.label, "test")

    def test_instantiate_with_unknown_type(self):
        with NamedTemporaryFile() as context:
            path = Path(context.name)
            path.write_text(self.UNKNOWN_LAUNCHER)

            launcher = self.core.__instantiate__(
                path, Launchers, GenericLauncher
            )
            self.assertIsInstance(launcher, GenericLauncher)
            self.assertEqual(launcher.label, "test")

    def test_instantiate_with_missing_type_option(self):
        with NamedTemporaryFile() as context:
            path = Path(context.name)
            path.write_text("[metadata]\nidentifier = test")

            with self.assertLogs(level="INFO") as context:
                result = self.core.__instantiate__(
                    path, Launchers, GenericLauncher
                )

            self.assertIsNone(result)
            self.assertIn(
                "ERROR:geode_gem.core:The option 'type' is missing from the "
                f"section 'metadata' in the file '{path}'",
                context.output,
            )

    def test_instantiate_with_exception(self):
        with NamedTemporaryFile() as context:
            path = Path(context.name)
            path.touch()

            with self.assertLogs(level="WARNING") as context:
                result = self.core.__instantiate__(
                    path, Launchers, GenericLauncher
                )

            self.assertIsNone(result)
            self.assertIn(
                f"WARNING:geode_gem.core:Cannot parse configuration '{path}': "
                "No section: 'metadata'",
                context.output,
            )

    def test_property_collections(self):
        self.quick_profile()
        self.assertEqual(len(self.core.collections), 1)
        self.assertEqual(self.core.collections[0].identifier, "test")

    def test_property_launchers(self):
        self.quick_profile()
        self.assertEqual(len(self.core.launchers), 1)
        self.assertEqual(self.core.launchers[0].identifier, "test")

    def test_commit_database(self):
        self.quick_profile()
        self.core.database.insert(
            "games", {"hash": "test", "label": "Test"}, commit=False
        )
        self.core.commit_database()

        results = self.core.database.select(
            "games", ["label"], where=["hash = test"]
        )
        self.assertEqual(results[0]["label"], "Test")

    def test_get_collection_from_valid_identifier(self):
        self.quick_profile()
        self.assertEqual(self.core.get_collection("test").identifier, "test")

    def test_get_collection_from_unknown_identifier(self):
        self.quick_profile()
        self.assertIsNone(self.core.get_collection("unknown"))

    def test_get_launcher_from_valid_identifier(self):
        self.quick_profile()
        self.assertEqual(self.core.get_launcher("test").identifier, "test")

    def test_get_launcher_from_unknown_identifier(self):
        self.quick_profile()
        self.assertIsNone(self.core.get_launcher("unknown"))

    def test_get_path_from_config(self):
        self.quick_profile()
        self.assertEqual(
            self.core.get_path_from_config("test"),
            self.directory.joinpath("config", "default", "test"),
        )

    def test_get_path_from_data(self):
        self.quick_profile()
        self.assertEqual(
            self.core.get_path_from_data("test"),
            self.directory.joinpath("data", "default", "test"),
        )

    def test_read_profile_without_data(self):
        self.assertIsNone(self.core.profile_config_path)

        with self.assertLogs(level="DEBUG") as context:
            self.core.read_profile()

        self.assertTrue(self.core.profile_config_path.exists())
        self.assertEqual(self.core.profile_name, "default")
        self.assertEqual(len(self.core.store_collections), 0)
        self.assertEqual(len(self.core.store_launchers), 0)
        self.assertIn(
            "DEBUG:geode_gem.core:Prepare the profile default for reading",
            context.output,
        )

    def test_read_profile_with_collection_and_launcher(self):

        with self.assertLogs(level="DEBUG") as context:
            self.quick_profile()

        self.assertEqual(len(self.core.store_collections), 1)
        self.assertEqual(len(self.core.store_launchers), 1)
        self.assertIn(
            "DEBUG:geode_gem.core:Register the collection test", context.output
        )
        self.assertIn(
            "DEBUG:geode_gem.core:Register the launcher test", context.output
        )

    def test_read_another_profile(self):
        self.quick_profile("another-profile")
        self.assertEqual(len(self.core.store_collections), 1)
        self.core.read_profile()
        self.assertEqual(len(self.core.store_collections), 0)

    def test_save_game(self):
        self.quick_profile()
        game = GenericGame("game-test")
        self.core.save_game(game)

    def test_set_path_for_game(self):
        self.quick_profile()
        game = GenericGame("game-test")
        game.collection = self.core.collections[0]

        self.assertEqual(
            self.core.set_path_for_game(game, "folder", "txt"),
            self.directory.joinpath(
                "data",
                "default",
                "folder",
                game.collection.identifier,
                f"{game.identifier}.txt",
            ),
        )

    def test_update_game_from_database_without_data_or_launcher(self):
        self.quick_profile()
        game = GenericGame("game-test")
        game.collection = self.core.collections[0]

        self.core.update_game_from_database(game)
        self.assertEqual(
            game.log_path,
            self.directory.joinpath(
                "data",
                "default",
                "logs",
                game.collection.identifier,
                f"{game.identifier}.log",
            ),
        )

    def test_update_game_from_database(self):
        self.quick_profile()
        game = GenericGame("game-test")
        game.collection = self.core.collections[0]
        game.launcher = "test"
        game.calc_checksum()

        self.core.database.insert(
            "games", {"hash": game.checksum, "label": "Awesome Game"}
        )

        self.core.update_game_from_database(game)
        self.assertEqual(game.label, "Awesome Game")
        self.assertEqual(game.launcher.identifier, "test")
