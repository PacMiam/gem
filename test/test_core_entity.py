# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from configparser import NoOptionError, NoSectionError
from pathlib import Path
from unittest import TestCase

from geode_gem.core.entity import Entity
from test.utils import generate_config


VALID_CONFIG = """[metadata]
identifier = this-is-a-game
label = Awesome Game
icon = x-terminal

[environment]
DISPLAY = :0
"""


class EntityTestCase(TestCase):
    def test_constructor_with_identifier_only(self):
        entity = Entity("test")
        self.assertEqual(entity.identifier, "test")
        self.assertEqual(entity.label, "test")

    def test_constructor_with_label_parameter(self):
        entity = Entity("test", label="Test")
        self.assertEqual(entity.label, "Test")
        self.assertEqual(entity.icon, "test")

    def test_constructor_with_icon_parameter(self):
        entity = Entity("test", icon=Path("some-cover"))
        self.assertEqual(entity.icon.stem, "some-cover")

    def test_constructor_from_configuration_with_missing_metadata(self):
        parser = generate_config("")
        with self.assertRaises(NoSectionError):
            Entity.new_from_configuration(parser)

    def test_constructor_from_configuration_with_missing_identifier(self):
        parser = generate_config("[metadata]")
        with self.assertRaises(NoOptionError):
            Entity.new_from_configuration(parser)

    def test_constructor_from_configuration_with_empty_identifier(self):
        parser = generate_config("[metadata]\nidentifier = ")
        with self.assertRaises(KeyError):
            Entity.new_from_configuration(parser)

    def test_constructor_from_configuration_with_empty_label(self):
        parser = generate_config("[metadata]\nidentifier = test\nlabel = ")
        entity = Entity.new_from_configuration(parser)
        self.assertEqual(entity.label, entity.identifier)

    def test_constructor_from_configuration_with_valid_data(self):
        parser = generate_config(VALID_CONFIG)
        entity = Entity.new_from_configuration(parser)
        self.assertEqual(entity.identifier, "this-is-a-game")
        self.assertEqual(entity.label, "Awesome Game")
        self.assertEqual(entity.icon, "x-terminal")
        self.assertEqual(entity.getenv("DISPLAY"), ":0")
        self.assertIsNone(entity.getenv("display"))

        parser = generate_config("[metadata]\nidentifier = emacs")
        entity = Entity.new_from_configuration(parser)
        self.assertEqual(entity.identifier, "emacs")
        self.assertEqual(entity.label, entity.identifier)
        self.assertEqual(entity.icon, entity.identifier)

    def test_add_environment_variable(self):
        entity = Entity("test")
        self.assertEqual(len(entity.environment), 0)
        entity.putenv("TEST", "Hello")
        self.assertCountEqual(entity.environment, {"TEST": "Hello"})

    def test_get_environment_variable(self):
        entity = Entity("test")
        entity.putenv("TEST", "Hello")
        self.assertEqual(entity.getenv("TEST"), "Hello")

    def test_get_environment_variable_with_unknown_variable(self):
        self.assertIsNone(Entity("test").getenv("UNKNOWN"))

    def test_get_environment_variable_with_unknown_variable_and_default(self):
        self.assertEqual(Entity("test").getenv("UNKNOWN", "default"), "default")

    def test_remove_environment_variable(self):
        entity = Entity("test")
        entity.putenv("TEST", "Hello")
        self.assertEqual(len(entity.environment), 1)
        entity.unsetenv("TEST")
        self.assertEqual(len(entity.environment), 0)

    def test_remove_environment_variable_with_unknown_variable(self):
        with self.assertRaises(KeyError):
            Entity("test").unsetenv("UNKNOWN")

    def test_remove_environment_variable_with_ignore_missing_flag(self):
        Entity("test").unsetenv("UNKNOWN", ignore_missing=True)

    def test_entity_as_string(self):
        self.assertEqual(str(Entity("test")), "test")
