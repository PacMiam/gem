# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest import TestCase

from geode_gem.core.configuration import Configuration


class ConfigurationTestCase(TestCase):
    BASIC_CONFIG = """
    [test]
    data = 1,2,3
    file = test.txt
    """

    def test_constructor(self):
        config = Configuration()
        self.assertEqual(len(config.sections()), 0)

    def test_constructor_with_file_path(self):
        with NamedTemporaryFile() as context:
            path = Path(context.name)
            path.write_text(self.BASIC_CONFIG)

            config = Configuration(path)
            self.assertEqual(config.get("test", "data"), "1,2,3")

    def test_constructor_with_directory_path(self):
        with TemporaryDirectory() as path:
            with self.assertRaises(IsADirectoryError) as context:
                Configuration(Path(path))

        self.assertEqual(
            context.exception.args[0],
            "Cannot use a directory to set the configuration instance",
        )

    def test_getlist(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        self.assertCountEqual(
            config.getlist("test", "data", separator=","), ["1", "2", "3"]
        )

    def test_getpath(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        path = config.getpath("test", "file")
        self.assertEqual(path.stem, "test")
        self.assertEqual(path.suffixes, [".txt"])

    def test_save_to_directory(self):
        with TemporaryDirectory() as path:
            with self.assertRaises(IsADirectoryError) as context:
                Configuration().save(Path(path))

        self.assertEqual(
            context.exception.args[0],
            f"Cannot use the path '{path}' to save the configuration "
            "since its a directory",
        )

    def test_save_to_unknown_file(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        with TemporaryDirectory() as path:
            filepath = Path(path, "test.ini")
            self.assertFalse(filepath.exists())
            config.save(filepath)
            self.assertTrue(filepath.exists())
            self.assertEqual(
                filepath.read_text(),
                "[test]\ndata = 1,2,3\nfile = test.txt\n\n",
            )

    def test_save_to_existing_file_without_forcing(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        with NamedTemporaryFile() as path:
            filepath = Path(path.name)
            filepath.write_text("test")
            self.assertTrue(filepath.exists())

            with self.assertRaises(FileExistsError) as context:
                config.save(filepath)

        self.assertEqual(
            context.exception.args[0],
            f"Cannot save the configuration in the path '{filepath}' since "
            "this file already exists on the filesystem and the argument "
            "force_if_exists was not used",
        )

    def test_save_to_existing_file_with_forcing(self): ...
